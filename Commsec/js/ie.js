﻿$(function () {
    $('[autofocus]').focus();

    //Required for custom checkbox styling
    $('.cbToggle').change(function () {
        this.checked ? $(this).addClass('checked') : $(this).removeClass('checked');
    });
});