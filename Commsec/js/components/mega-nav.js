﻿$(function () {

    $('.hamburger').on('click', function () {

        if (!$('.wrapper').hasClass('reveal-left')) {
            $('html, body').animate({ scrollTop: 0 }, 300, function () {
                $('.wrapper').addClass('reveal-left');
            });
        }
        else {
            console.log('removing class');
            $('.wrapper').removeClass('reveal-left');
        }
    });

    $('.hidden-xs .nav-search input[type="text"]').on('focus blur', function () {
        $('.global-nav.hidden-xs').toggleClass('reveal-search');
    });

    $('.visible-xs .nav-search').on('click', function () {
        $('.global-nav.visible-xs').toggleClass('reveal-search');
        return false;
    });
    
    //link to mobile login
    
    if($('#mobile-login-form').length > 0)
	{
    	$('#mobile-login-button').addClass('active-login-icon');
	}
    
    $('#mobile-login-button').click(function(e){ 
		if($('#mobile-login-button').hasClass('active-login-icon'))
		{
			window.history.back();
		}
		else
		{
			window.location.href = "/content/commsec/en/mobile/login.html";
		}
		return false;
	});
    

    $('.global-nav .nav').on('mouseenter', '> .ng-scope', function () {
        $('body').addClass('mega-nav-active');
    });
    $('.module-overlay').on(
        {
            mouseenter: function () {
                $('body').removeClass('mega-nav-active');
            }, click: function () {
                $('.wrapper').removeClass('reveal-left');
            }
        }
    );
});