﻿$(function () {

    var controller = new ScrollMagic();

    var scene1 = new ScrollScene({ duration: 1000, offset: 0 })
        .setPin('#section0')
        .addTo(controller)
        .on('progress', function (evt) {
            console.log('progress', evt.target);
        })
        .sceneIndex = 0;

    var scene2 = new ScrollScene({ duration: 1000, triggerElement: '#section1', offset: 429 })
            .setPin('#section1')
            .addTo(controller)
            .on('progress', function (evt) {
                console.log('progress', evt.target);
            })
            .on('enter', function(evt) {
                console.log('ENTER');
            })
            .sceneIndex = 0;

    var scene3 = new ScrollScene({ duration: 1000, triggerElement: '#section2', offset: 429 })
            .setPin('#section2')
            .addTo(controller)
            .on('progress', function (evt) {
                console.log('progress', evt.target);
            })
            .sceneIndex = 0;

    var update = function () {
        console.log('UPDATE');
        var windowHeight = $(window).height();
        console.log(windowHeight);

        //$('.scene .container .css-table').css('height', windowHeight - 90 + 'px');
        //controller.update();
    };

    $(window).on('resize', function () {
        update();
    });

    update();

});