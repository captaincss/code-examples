﻿$(function () {
    var $announcements = $('section.announcements');
    var debounced = helpers.debounce(function () {
        if (helpers.isTouch() && $announcements.length > 0) {
            announcementsTouchScroll();
        }
    }, 250);
    $(document).on('scroll', debounced);

    function announcementsTouchScroll() {
        var scrollPos = $(document).scrollTop();

        if (helpers.isMobile()) {
            // MOBILE
            if (scrollPos < $announcements.height()) {
                $announcements.show();
            }
            else {
                $announcements.hide();
            }
        }
        else {
            // TABLET
            var $globalNav = $('.global-nav-werapper');

            if ($globalNav.hasClass('affix-top') || scrollPos == 0) {
                $announcements.show();
            }
            else {
                $announcements.hide();
            }
        }
    }
});