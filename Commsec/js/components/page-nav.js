$(function () {

    /**
    * Toggling of mobile side-nav
    */
    mobilePageNav = function () {
        var $click = $('.page-nav-mob a.toggle'),
            $panel = $('.page-nav-mob .panel'),
            $share = $('.share-box-inner');

        function collapse() {
            $panel.animate({ right: '-186px' });
            $click.animate({ right: '0px' }, function () {
                $('.share-box-inner').removeClass('hidden');
            }).text('MORE').removeClass('minus').addClass('plus');
        }

        function navToggle() {
            if ($panel.css('right').substring(0, 1) == '-') {

                // collapsed --> expanded
                $panel.animate({ right: '0px' });
                $click.animate({ right: '186px' }).text('LESS').removeClass('plus').addClass('minus');
            }
            else {
                collapse();
            }
        };

        function myMobileNav () {
            $click.on('click', function () {
                $('.share-box-inner').toggleClass('hidden');
                navToggle();
                return false;
            });
        };

        if ($click.length > 0)
        {
            $panel.find('a').on('click', function (e) {
                // will fire when user clicks on nav item
                collapse();
                $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - 15 }, 1000);
                e.preventDefault();
            });

            myMobileNav();
            $panel.on('click', function () { navToggle(); }); // will close when user clicks on panel ...
        }
    }

    $('#nav.scrollspy a').on('click', function (e) {
        positionNav($(this).attr('href'), e);

        e.preventDefault();
    });

    function positionNav(targetEle, event) {
        
        var offsetHeight = 0,
            href = (targetEle) ? targetEle : window.location.hash,
            eleOffsetTop = (href && $(href).offset()) ? $(href).offset().top : 0,
            scrollPosition;

        $('[data-spy="affix"]').each(function (index, ele) {
            offsetHeight += $(ele).height();
        });

        if ($(document).scrollTop() >= $('.page-nav').data('affixOffset'))
            scrollPosition = eleOffsetTop - offsetHeight;
        else
            scrollPosition = eleOffsetTop - (offsetHeight * 2) + 57;

        scrollPosition += 2;

        try {
            //window.location.hash = href;
            if ($('.page-nav').hasClass('no-animate')) {
                $(document).scrollTop(scrollPosition);
            }
            else {
                //$(document).scrollTop(scrollPosition);
                $('html, body').animate({ scrollTop: scrollPosition }, 300, function () {
                    if (event) {
                        console.log(event);
                        $('.page-nav.scrollspy li').removeClass('active');
                        $(event.currentTarget).parent('li').addClass('active');
                    }
                });
            }
        }
        catch (e) {
            console.log(e, 'Unable to scroll to position');
        }
    }

    /* Used for scrollspy - Offset value is the height of the mega nav + page nav */
    $('body').scrollspy({ target: '#nav.scrollspy', offset: 157 });

    function checkDocumentHeight(callback) {
        var lastHeight = document.body.clientHeight, newHeight, timer;
        (function run() {
            newHeight = document.body.clientHeight;
            if (lastHeight != newHeight)
                callback();
            lastHeight = newHeight;
            timer = setTimeout(run, 50);
        })();
    }

    function refreshScrollspy() {
        $('body').scrollspy('refresh').data()['bs.scrollspy'].process();
    }

    checkDocumentHeight(refreshScrollspy);

    /* Handling of pull-out nav for mobile-only */
    mobilePageNav();

    window.onload = function (e) {
        positionNav(null);
    };

});