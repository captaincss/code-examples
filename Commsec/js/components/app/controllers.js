﻿var commSecApp = angular.module('commSec', ['commSecServices', 'ngSanitize']);

commSecApp.controller('SiteController', ['$scope', '$sanitize', 'SiteNavData', function ($scope, $sanitize, SiteNavData) {
    var rawnav = [],
        columnData = [];

    //Call nav service to retreive nav
    SiteNavData.query().$promise.then(function (data) {
        rawnav = data;
        columnData = data;
        $scope.rawnav = rawnav;
        $scope.columnData = columnData;

        //console.log('SERVICE', $scope.columnData);
    });

    $scope.activeClass = function (rptString, bodyString, className) {
        if ($sanitize(rptString) == $sanitize($('body').attr(bodyString))) {
            return className;
        }
        else {
            return '';
        }
    }

    $scope.Math = window.Math;

    $scope.trim = function (input, strLength) {
        if (input.length >= strLength) {
            return input.substr(0, strLength) + '...';
        }
        else {
            return input;
        }
    }

    $scope.uploadSupported = function(){
        if (window.FormData === undefined)
        {
            return false;
        }
        return true;
    }
}]);

commSecApp.controller('MegaNavController', ['$scope', function ($scope) {
    $scope.nav = [],
    MAX_COLS = 4;

    $scope.$watch('columnData', function () {
        if ($scope.columnData) {
            $scope.nav = reshapeData();
        }
    });
    //Reshape the supplied JSON to be appropriate format for rendering
    function reshapeData() {
        var data = $scope.columnData,
            reshaped = [],
            cols = [];

        //Create primary columns

        for (var i = 0; i < data.length; i++) {
            var subCols = [],
                availColumns = MAX_COLS,
                dataObj = data[i].pageInfo,
                childPages = data[i].childPages,
                startIndex = 0;

            cols[i] = {};
            cols[i].title = dataObj.jcrTitle;

            //If you have an overview column, create it
            if (dataObj.hasOwnProperty('primaryNavigationRichText')) {
                var column = {};

                column.overview = jQuery('<div />').html(dataObj.primaryNavigationRichText).text();
                subCols[0] = column;
                delete dataObj.primaryNavigationRichText;
                availColumns -= 1;
                startIndex += 1;
            }

            //If you have an image feature column, create it
            if (dataObj.hasOwnProperty('primaryNavigationImagePath')) {
                var column = {};

                column.featureImage = { 'path': dataObj.primaryNavigationImagePath, 'link': dataObj.primaryNavigationImageLink };
                subCols[MAX_COLS - 1] = column;
                delete dataObj.primaryNavigationImagePath;
                availColumns -= 1;
            }

            //Create remaining columns
            var linksPerCol = Math.ceil(childPages.length / availColumns),
                columnCreateCount = availColumns,
                startItemIndex = 0;

            for (var j = 0; j < availColumns; j++) {
                var column = {};
                column.subItems = [];

                //Create links
                for (var k = 0; k < linksPerCol; k++) {
                    if (childPages[startItemIndex])
                        column.subItems[k] = { 'title': childPages[startItemIndex].pageInfo.jcrTitle, 'link': childPages[startItemIndex].pageInfo.pagePath, 'description': jQuery('<div />').html(childPages[startItemIndex].pageInfo.primaryNavigationDescription).text() };
                    startItemIndex++;
                    //childPages.shift();
                }

                subCols[startIndex++] = column;
                columnCreateCount -= 1;
            }

            cols[i].subCols = subCols;

        }


        return cols;

    }

}]);

commSecApp.controller('SiteNavController', ['$scope', function ($scope) { }]);

commSecApp.controller('AnnouncementsController', ['$scope', '$timeout', 'AnnouncementsData', function ($scope, $timeout, AnnouncementsData) {
    AnnouncementsData.query().$promise.then(function (data) {
        $scope.rawData = data;
        /*
            reshape service data so that only items which have not been dismissed are returned to customer (if cookie exists)
        */
        $scope.announcements = [];
        $scope.cookieName = 'announcements';
        var cookieVal = helpers.readCookie($scope.cookieName);

        if (cookieVal) {
            for (var i = 0; i < $scope.rawData.length; i++) {
                var item = $scope.rawData[i];
                if (!helpers.findMatchinCommaSeparated(cookieVal, item.id)) {
                    $scope.announcements.push(item);
                }
            }
        }
        else {
            $scope.announcements = $scope.rawData;
        }

        $scope.flagP3();

        $('.announcements').removeClass('hidden');
    });

    $scope.removeItem = function (item, $event) {

        var itemLength = $scope.announcements.length,
            $this = $($event.target),
            $parent = $this.parents('.announcement'),
            timeInterval = 600;

        if (itemLength > 1) {
            $parent.fadeOut(timeInterval);
            deleteItem(item, timeInterval);
        }
        else {
            $parent.fadeTo(timeInterval, 0).slideUp(timeInterval);
            deleteItem(item, 1000);
        }


        function deleteItem(item, timeInterval) {
            $timeout(function () {
                helpers.createCookieCommaSeparated($scope.cookieName, item.id, 365);
                $scope.announcements.splice($scope.announcements.indexOf(item), 1); // next item will become first item so will therefore be visible ...
                $scope.flagP3();
            }, timeInterval);
        }
    }

    $scope.isP1 = function (item) {
        if (item.priority == 1)
            return true;
        else
            return false;
    }

    $scope.flagP3 = function () {
        // Priority 3 items should only ever be seen once (whether closed or not), so set Cookie regardless
        if ($scope.announcements.length > 0)
        {
            if ($scope.announcements[0].priority == 3) {
                helpers.createCookieCommaSeparated($scope.cookieName, $scope.announcements[0].id, 365);
            }
        }
    }
}]);

commSecApp.controller('FeedbackController', ['$scope', 'FeedbackService', function ($scope, feedbackService) {
    var setValue,
        displayTexts = ['Was this answer helpful?', 'Thank you for your feedback', 'Thank you for your comments'];
    $scope.displayText = displayTexts[0];
    $scope.helpful;
    $scope.feedback = '';
    $scope.pagePath= $("#pagePath").val();
    $scope.pageName= $("#pageName").val();
    $scope.componentPath= $("#componentPath").val();
    $scope.changed = false;
    $scope.submitted = false;
	$scope.noLabelCss = '';
	$scope.yesLabelCss = '';

    $scope.$watch('helpful', function () {
        if ($scope.helpful && !$scope.changed) {
            $scope.displayText = displayTexts[1];
            $scope.changed = true;
            setValue = $scope.helpful;
            $scope.submit();
        }
        else {
            $scope.helpful = setValue;
        }
    });

    $scope.labelClick = function (val) {
        $scope.helpful = val;
		if(!$scope.changed){
			if(val === 'yes'){
				$scope.yesLabelCss = 'yes';
				$scope.noLabelCss = '';
			}else if(val === 'no'){
				$scope.yesLabelCss = '';
				$scope.noLabelCss = 'no';
			}else{
				$scope.yesLabelCss = '';
				$scope.noLabelCss = '';
			}
		}
    };

    $scope.submit = function () {
        if (!$scope.submitted && $scope.feedback) {
            $scope.submitted = true;
            $scope.displayText = displayTexts[2];
        }
        feedbackService.query({ helpful: $scope.helpful, feedback: $scope.feedback, pagePath: $scope.pagePath, pageName: $scope.pageName, componentPath: $scope.componentPath });
    };
}]);

commSecApp.controller('ContactUsController', ['$scope', '$http', '$sanitize', 'PostFactory', function ($scope, $http, $sanitize, PostFactory) {

    $scope.resetForm = function () {
        if ($scope.user) {
            $scope.user.title = '';
            $scope.user.firstName = '';
            $scope.user.surname = '';
            $scope.user.clientId = '';
            $scope.user.email = '';
            $scope.user.phone = '';
            $scope.user.enquiry = '';
            $scope.user.subject = '';
            $scope.user.comment = '';
            $scope.user.attach = '';
        }
    };

    $scope.cancel = function () {
        $scope.resetForm();
        $scope.contactUs.$valid;
        $scope.contactUs.$setPristine();
        $scope.trySubmit = false;
        $scope.invalidFile = false;
        $scope.isFinished = false;
        $scope.fileList = [];
        $scope.animate();
    }

    $scope.processForm = function () {
        $scope.formData = {};
        $scope.formData.user = $scope.user;
        $scope.formData.attachments = $scope.fileList || []; // object contains form input data and file upload ...

        PostFactory('/Handlers/ContactUs.ashx', $scope.formData, 'multipart/form-data')
            .success(function (data) {
                console.log('SUCCESS', data);
            }).
            error(function (error) {
                console.log('ERROR', error);
            });
    }

    $scope.validate = function () {
        $scope.trySubmit = true;

        console.log('form has been submitted?', '$scope.trySubmit', $scope.trySubmit);
        console.log('form is valid?', '$scope.contactUs.$valid', $scope.contactUs.$valid);

        $scope.invalidFile = false;

        $scope.invalidFileSize(2048);
        if ($scope.user) {
            $scope.invalidFileExtension($scope.user.attach || '');
        }
        else {
            $scope.validFileExtension = true;
        }

        if (!$scope.validFileExtension || !$scope.validFileSize) {
            $scope.invalidFile = true;
        }

        console.log('$scope.validFileExtension', $scope.validFileExtension);
        console.log('$scope.validFileSize', !$scope.validFileSize)
        console.log('attachment is invalid?', '$scope.invalidFile', $scope.invalidFile);

        if (!$scope.isFaq && ($scope.contactUs.$valid && !$scope.invalidFile && $scope.trySubmit)) {
            $scope.isFaq = true;
            console.log('show FAQs');

            //$scope.bindFaqs();
            $scope.getFaqData();

            return false;
        }

        if ($scope.isFaq && ($scope.contactUs.$valid && !$scope.invalidFile && $scope.trySubmit)) {
            // form has been sumbitted, passes angularJs validation and file upload is 'valid'
            console.log('form data', $scope.user);
            console.log('attachment object?', '$scope.fileList', $scope.fileList);
            $scope.processForm();
            console.log('show confirmation message');
            $scope.isFaq = false;
            $scope.isFinished = true;
            $scope.animate();
            return false;
        }
        $scope.animate();
    };

    $scope.animate = function () {
        $('html, body').animate({ scrollTop: $('#contact-us').offset().top - 156 }, 'fast');
    }

    $scope.invalidFileSize = function (limit) {

        $scope.validFileSize = true;

        if (document.getElementById('fil-attach').files) {
            if (typeof FileReader)
            {
                if (document.getElementById('fil-attach').files.length > 0) {
                    var size = document.getElementById('fil-attach').files[0].size;
                    var kb = size / 1024; // size will be in 'bytes', so convert to KB

                    if (kb > limit) // limit set in View (KB)
                    {
                        $scope.validFileSize = false;
                        return true;
                    }
                    else {
                        $scope.attachment = document.getElementById('fil-attach').files[0]; // contains all necesssary binary info for file upload??
                    }
                }
            }
        }
        return false;
    }

    $scope.invalidFileExtension = function (file) {

        $scope.validFileExtension = true;

        var validFileExtensions = ['.doc', '.docx', '.pdf', '.jpg', '.gif', '.png'];
        if (file) {
            var blnValid = false;
            for (var j = 0; j < validFileExtensions.length; j++) {
                var sCurExtension = validFileExtensions[j];
                if (file.substr(file.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                //$scope.invalidFile = true;
                $scope.validFileExtension = false;
                return true;
            }
        }
        return false;
    }

    window.setInterval(function () {
        // poll file list ... (for uploading files)
        $scope.getUploadedFiles();
    }, 500);

    $scope.getUploadedFiles = function () {
        if ($scope.user) {
            if ($scope.user.attach) {
                if (!$scope.tempFile) {
                    appendToList();
                }
                else {
                    if ($scope.tempFile != $scope.user.attach) {
                        appendToList();
                    }
                }
            }
        }

        function appendToList() {
            $scope.invalidFileSize(2048);
            $scope.invalidFileExtension($scope.user.attach);

            $scope.invalidFile = false;

            if (!$scope.validFileExtension || !$scope.validFileSize) {
                $scope.invalidFile = true;
            }

            if (!$scope.invalidFile) {
                if (!$scope.fileList) {
                    $scope.fileList = [];
                }
                $scope.fileList.push(document.getElementById('fil-attach').files[0]);
                $scope.$apply(); // re-bind repeater ...
                $('.attach-container span').removeClass('invalidFile'); //TODO: user angularJS to do this???

                // then finally assign temp variable to value of input.
                $scope.tempFile = $scope.user.attach;
            }
            else {
                $('.attach-container .form-label').addClass('invalidFile'); //TODO: user angularJS to do this???
            }
        }
    };

    $scope.removeItem = function (idx, event) {
        var item_to_delete = $scope.fileList[idx];
        $scope.fileList.splice(idx, 1);
        event.preventDefault();
    };

    $scope.getFaqData = function () {
        /*var faqData = {
            'm': 'faq',
            'q': $scope.user.subject + ' ' + $scope.user.comment
        };*/
        var faqData = 'm=contactus&q=' + $scope.user.subject + ' ' + $scope.user.comment;
        PostFactory('/services/support/content/search/Search', faqData, 'application/x-www-form-urlencoded')
        //PostFactory('/Handlers/SearchServlet.ashx', faqData, 'application/x-www-form-urlencoded')
            .success(function (data) {
                console.log('SUCCESS', data);

                if (data.length == 0) {
                    console.log('NO RESULTS');
                    $scope.isFaq = false;
                    $scope.isFinished = true;
                }
                else {
                    $scope.faqData = data;
                }
            }).
            error(function (error) {
                console.log('ERROR', error);
                $scope.isFaq = false;
                $scope.isFinished = true;
            });
    }
}]);


commSecApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter, { 'event': event });
                });

                event.preventDefault();
            }
        });
    };
});