var commSecServices = angular.module('commSecServices', ['ngResource']);

commSecServices.factory('SiteNavData', ['$resource',
    function ($resource) {
        return $resource('/content/commsec/en/sitemap-data/data.json', {}, {
            query: { method: 'GET', isArray: true }
        });
    }]);

commSecServices.factory('AnnouncementsData', ['$resource',
    function ($resource) {
        return $resource('/content/commsec/en/_config/announcements.json', {}, {
            query: { method: 'GET', isArray: true }
        });
    }]);

commSecServices.factory('FeedbackService', ['$resource',
    function ($resource) {
        return $resource('/services/support/content/FeedBack/FeedBackPath', {}, {
            query: { method: 'GET', isArray: false }
        });
    }]);

commSecServices.factory('PostFactory', ['$http', function ($http) {

    return function (url, inputs, encoding) {
        $http.defaults.headers.post['Content-Type'] = encoding;
        console.log('inputs', inputs);
            return $http.post(url, inputs);
        };
    }]);

