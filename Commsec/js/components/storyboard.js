﻿/* 
 * Storyboard - New to Trading
 */

var sm;

$(function () {
    var storyboard = function () {
        var sceneElements = $('.scene');

        var SceneManager = function () {

            this.init = function () {
                console.log('init');
            }

            this.activeElement = 0;
            this.oldActiveElement = 0;
            this.fixedOffsetTop = 156;
            this.scrollRange = 325;
            this.numAnimationsInScene = 0;
            this.isPauseable = false;
            this.isPauseComplete = false;
            this.isCurrentlyPaused = false;
            this.currPlayIndex = 0;

            this.playNext = function () {
                if (_isAnimating == false && this.numAnimationsInScene > this.currPlayIndex) {
                    sceneAnimations[this.activeElement][this.currPlayIndex++]();
                }
            }

            this.resetPlayIndex = function () {
                //if ((this.numAnimationsInScene == this.currPlayIndex) && _isAnimating == false) {
                this.currPlayIndex = 0;
                //}
            }

            //Move to/play logic of scenes
            this.moveToNext = function () {
                console.log('current play index:', this.currPlayIndex);
                var currScrollPos = scroller.currScrollPos();
                var currTop = $(sceneElements[this.activeElement]).get(0).offsetTop - this.fixedOffsetTop;

                if (scroller.scrolledUp == true) // we are scrolling up ...
                {
                    //TODO: test scrolling up behaviour

                    if (currScrollPos == currTop) {
                        // nothing
                    }

                    if (currScrollPos < currTop) {
                        // nothing
                    }

                    if ((currScrollPos > currTop) && (currScrollPos < currTop + this.scrollRange)) {
                        this.animateTo(currTop);
                        console.log('snap');
                    }

                }
                else {
                    console.log('scrolled down', currScrollPos, currTop);
                    if (currScrollPos == currTop) {
                        // play animation ??
                        if (!this.isCurrentlyPaused) {
                            this.resetPlayIndex();
                            this.isCurrentlyPaused = true;
                        }


                        if (this.isPauseable && this.isPauseComplete == false) {
                            scroller.disable_scroll();
                            if (_isAnimating == false && this.numAnimationsInScene > this.currPlayIndex && this.isPauseComplete == false) {
                                this.playNext(); // current animation has finished, and we have more to come, so move to next animation
                            }
                            //if ((sm.numAnimationsInScene == currPlayIndex) && _isAnimating == false) {
                            if ((this.numAnimationsInScene == this.currPlayIndex) && _isAnimating == false) {
                                scroller.enable_scroll();
                                this.resetPlayIndex();
                                $(sceneElements[this.activeElement]).data('scroll-pause-complete', true);
                            }

                        }
                    }


                    else if (currScrollPos < currTop && (currScrollPos <= currTop - this.scrollRange)) {
                        // nothing
                        console.log('nothing');
                    }

                    else if ((currScrollPos < currTop) && (currScrollPos >= currTop - this.scrollRange) && !_isAnimating) {

                        console.log('snap down');
                        this.isCurrentlyPaused = false;
                        $(sceneElements[this.activeElement]).data('scroll-pause-complete', false);

                        var nextOffset = $(sceneElements[this.activeElement]).get(0).offsetTop - this.fixedOffsetTop;
                        console.log('next offset:', nextOffset);
                        this.animateTo(currTop);

                        if (_isAnimating == false && this.isPauseable && this.isPauseComplete == false) {
                            //scroller.disable_scroll();
                            this.resetPlayIndex();
                            this.playNext();
                        }

                    }


                    else {
                        if (this.currPlayIndex < this.numAnimationsInScene && !this.isPauseable)
                            this.playNext();
                    }

                }
            }

            this.animateTo = function (position) {
                /*_isAnimating = true;
                $('html, body').animate({ scrollTop: position }, 500, function () {
                    _isAnimating = false;
                });*/
                //$(document).scrollTop(position)
            }

            this.getIsPauseable = function () {
                if ($(sceneElements[this.activeElement]).data('scroll-pause')) {
                    this.isPauseable = true;
                }
                else {
                    this.isPauseable = false;
                }
            }

            this.getIsPauseComplete = function () {
                if (!$(sceneElements[this.activeElement]).data('scroll-pause-complete')) {
                    this.isPauseComplete = false;
                }
                else {
                    this.isPauseComplete = true;
                }
            }

            this.getNumAnimationsInScene = function () {
                this.numAnimationsInScene = (sceneAnimations[this.activeElement]) ? sceneAnimations[this.activeElement].length : 0;
            }

            this.getActiveScene = function () {

                /*
                *********************************
    
                - whilst scrolling, iterate through each DOM element (scene)
                - if current scroll position is greater than top position of current element in 'for' loop ...
    
                *********************************
                */

                var scrollPos = scroller.currScrollPos();

                for (var i = 0; i < sceneElements.length; i++) {
                    var offset = $(sceneElements[this.activeElement]).data('scroll-offset'),
                        totalOffset = 0;

                    totalOffset = $(sceneElements[this.activeElement]).data('scroll-offset') || this.fixedOffsetTop;

                    //console.log('comparing sp:' + scrollPos + ' to seOffset: ' + (sceneElements[i].offsetTop - totalOffset));

                    // if scrolled past top of current scene, or 'x' pixels above current scene ...
                    if (scrollPos >= (sceneElements[i].offsetTop - totalOffset) || (scrollPos + this.scrollRange) >= (sceneElements[i].offsetTop - totalOffset)) {

                        if (i > this.oldActiveElement) {
                            $(sceneElements[this.oldActiveElement]).data('scroll-pause-complete', false); // 're-activate' previous element ...
                            this.oldActiveElement = i;
                        }

                        this.activeElement = i; // *** we have our current element!
                        this.oldActiveElement = this.activeElement;
                    }
                }
                

                console.log('Active scene:', this.activeElement);

            }

            this.init();
        };


        var ScrollManager = function () {

            var self = this;
            this.currScrollPos = function () {
                return $(document).scrollTop();
            };

            this.hasScrolledUp = function (e) {
                if (e.originalEvent) {
                    if (e.originalEvent.wheelDelta >= 0)
                        this.scrolledUp = true;
                    else
                        this.scrolledUp = false;

                    $.event.trigger({
                        type: 'customScroll',
                        message: this.scrolledUp,
                        time: new Date()
                    });
                }

                return this.scrolledUp;

            };

            // left: 37, up: 38, right: 39, down: 40,
            // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
            var keys = [37, 38, 39, 40];

            function preventDefault(e) {
                $.event.trigger({
                    type: 'customScroll',
                    message: this.scrolledUp,
                    time: new Date()
                });
                e = e || window.event;
                if (e.preventDefault)
                    e.preventDefault();
                e.returnValue = false;
            }

            function keydown(e) {
                for (var i = keys.length; i--;) {
                    if (e.keyCode === keys[i]) {
                        preventDefault(e);
                        return;
                    }
                }
            }

            function wheel(e) {
                //self.hasScrolledUp(e);
                preventDefault(e);
            }

            this.disable_scroll = function () {
                console.log('disable scroll');
                sm.isCurrentlyPaused = true;
                if (window.addEventListener) {
                    window.addEventListener('DOMMouseScroll', wheel, false);
                }

                window.onmousewheel = wheel;
                document.onkeydown = keydown;
            }

            this.enable_scroll = function () {
                console.log('enable scroll');
                sm.isCurrentlyPaused = false;
                if (window.removeEventListener) {
                    window.removeEventListener('DOMMouseScroll', wheel, false);
                }
                window.onmousewheel = document.onmousewheel = document.onkeydown = null;
            }
        };

        var ScrollAnimator = function (options) {

            this.animEle = $(options.animEle),
            this.cssClasses = options.cssClasses,
            this.activeIndex = 0;

            this.change = function () {
                console.log('change animEle class, class to add: ', this.cssClasses[this.activeIndex + 1]);
                
                console.log('PAUSED: ', sm.isCurrentlyPaused);
                if (!sm.isCurrentlyPaused) {
                    this.animEle.removeClass('hidden');
                    this.animEle.removeClass(this.cssClasses[this.activeIndex++]);

                    if (this.activeIndex >= this.cssClasses.length)
                        this.activeIndex = 0;

                    this.animEle.addClass(this.cssClasses[this.activeIndex]);
                }
                else {
                    this.animEle.addClass('hidden');
                }
            }

        };

        sm = new SceneManager();
        var scroller = new ScrollManager();
        var sa = new ScrollAnimator({ animEle: '#coin-fixed', cssClasses: ['coin-flip-0', 'coin-flip-1', 'coin-flip-2', 'coin-flip-3', 'coin-flip-4', 'coin-flip-5', 'coin-flip-6', 'coin-flip-7', 'coin-flip-8', 'coin-flip-9', 'coin-flip-10', 'coin-flip-11'] });


        /*
        *********************************

        - throttle function required for Firefox, as FF implements 'smooth scrolling',
        - so we need to limit the occurancy of scroll event fires

        *********************************
        */

        function throttle(fn, timeout) {
            var tid = 0;
            return function () {
                clearTimeout(tid);
                var args = [].slice.call(arguments),
                    ctx = this;

                tid = setTimeout(function () {
                    fn.apply(ctx, args);
                }, timeout);
            };
        }

        var oldOffset = 0;
        //$(window).on('scroll', function () {
        $(window).on('scroll', throttle(function () {
            var newOffset = $(document).scrollTop();

            if (oldOffset < newOffset) {
                scroller.scrolledUp = false;
            }
            else {
                scroller.scrolledUp = true;
            }

            if (!_isAnimating) {
                $.event.trigger({
                    type: 'customScroll',
                    message: scroller.scrolledUp,
                    time: new Date()
                });
            }

            oldOffset = newOffset;
        }, 100));

        $(document).on('customScroll', function (evt) {

            sa.change(evt);
            sm.getActiveScene();
            sm.getNumAnimationsInScene();
            sm.getIsPauseable();
            sm.getIsPauseComplete();
            sm.moveToNext();

        });

    }();

});

/*
*********************************

- function called from animation (swiffy) to indicate end of animation
- '_isAnimating' = global variable

*********************************
*/

var _isAnimating = false;

function animationIsComplete(animation, isComplete) {
    console.log('ANIM COMPLETE');
    if (isComplete) {
        _isAnimating = false;

    }
}