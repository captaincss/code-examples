$(function () {

    var $share = $('.share-hero');
    var shareHtml = $share.first().outerHtml(); // get HTML for DOM element
    $share.remove(); // remove from DOM
    $('.global-nav-wrapper').after(shareHtml); // 'inject' HTML after nav
    $('.share-box').show(); // show (hidden in CSS)

    $(".email-page-link").click(function (e) {
        var emailText = $("#share-this-page-email-text").attr("value");
        if (emailText) {
            emailText = emailText.replace('#commsec-share-this-url#', '%0A%0A' + escape(window.location.href) + '%0A%0A');
        }
        var subj = escape($("#share-this-page-email-subject").attr("value"));
        if (subj) {
            window.location.href = "mailto:?subject=" + subj + "&body=" + emailText;
        } else {
            window.location.href = "mailto:?body=" + emailText;
        }
    });

    $('a[href*="#commsec-share-this-url#"]').attr('href', function (i, href) {
        return href.replace('#commsec-share-this-url#', '%0A%0A' + escape(window.location.href) + '%0A%0A');
    });

    var isTouch = helpers.isTouch();

    if (isTouch) {
        var $cta = $('.share-cta'),
            $list = $('.share-box-inner ul')

        $('html').on('click touchstart', function (e) {
            $list.hide();
            $list.removeClass('open');
        });

        $cta.on(
            'touchstart',
            function (e) {
                if ($list.hasClass('open')) {
                    $list.hide();
                    $list.removeClass('open');
                }
                else {
                    $list.show();
                    $list.addClass('open');
                }
                e.stopPropagation();
                e.preventDefault();
                return false;
            }
        );
    }

});
