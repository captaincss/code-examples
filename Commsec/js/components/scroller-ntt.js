﻿var f1Stage0;

$(function () {

    var s = skrollr.init({
        render: function (data) {
            //console.log(data);

        }
    });

    $(document).on('scroll', function () {
        var scrollTop = $(document).scrollTop();
        console.log('ST: ', scrollTop);

        if (scrollTop <= 100) {
            $('#coin-fixed').attr('class', 'coin coin-flip-0');
        }
        else if (scrollTop <= 200) {
            console.log('200');
            $('#coin-fixed').attr('class', 'coin coin-flip-1');
        }
        else if (scrollTop <= 300) {
            $('#coin-fixed').attr('class', 'coin coin-flip-2');
        }
        else if (scrollTop <= 400) {
            $('#coin-fixed').attr('class', 'coin coin-flip-3');
        }
        else if (scrollTop <= 500) {
            $('#coin-fixed').attr('class', 'coin coin-flip-4');
        }
        else if (scrollTop <= 600) {
            $('#coin-fixed').attr('class', 'coin coin-flip-5');
        }
        else if (scrollTop <= 700) {
            $('#coin-fixed').attr('class', 'coin coin-flip-6');
        }
        else if (scrollTop <= 800) {
            $('#coin-fixed').attr('class', 'coin coin-flip-7');
        }
        else if (scrollTop <= 900) {
            $('#coin-fixed').attr('class', 'coin coin-flip-8');
        }
        else if (scrollTop <= 1000) {
            $('#coin-fixed').attr('class', 'coin coin-flip-9');
        }
        else if (scrollTop <= 1100) {
            $('#coin-fixed').attr('class', 'coin coin-flip-10');
        }
        else if (scrollTop <= 1200) {
            $('#coin-fixed').attr('class', 'coin coin-flip-11');
        }
        else if (scrollTop <= 1300) {
            $('#coin-fixed').attr('class', 'coin coin-flip-12');
            $('#pie').attr('class', 'hidden');
        }
        else if (scrollTop <= 1400) {
            $('#coin-fixed').attr('class', 'hidden');
            $('#pie').attr('class', '');
        }
        else if (scrollTop <= 1500) {
            $('#pie').attr('class', 'pie-1');
        }
        else if (scrollTop <= 1600) {
            $('#pie').attr('class', 'pie-2');
        }
        else if (scrollTop <= 1700) {
            $('#coin-fixed').attr('class', 'hidden');
            $('#pie').attr('class', 'pie-3');
        }
        else if (scrollTop <= 1800) {
            $('#pie').attr('class', 'hidden');
            $('#coin-fixed').attr('class', 'coin coin-flip-1');
        }
        else if (scrollTop <= 1900) {
            $('#coin-fixed').attr('class', 'coin coin-flip-2');
        }
        else if (scrollTop <= 2000) {
            $('#coin-fixed').attr('class', 'coin coin-flip-3');
        }
        else if (scrollTop <= 2100) {
            
        }
        else if (scrollTop <= 2200) {
            
        }

    });

});