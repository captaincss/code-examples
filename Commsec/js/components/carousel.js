$(function () {
    if (helpers.isMobile()) {
        // on resize, if it's NOT mobile, remove the style attributes that may have been added by the slideToggle feature in mobile mode...
        reshapeHeadings([$('.hero-banner h1'), $('[class*="header-h1"]')], 6, 'small-heading');
    }

    var debounced = helpers.debounce(function () {
        if (helpers.isMobile()) {
            // on resize, if it's NOT mobile, remove the style attributes that may have been added by the slideToggle feature in mobile mode...
            reshapeHeadings([$('.hero-banner h1'), $('span [class*="header-h1"]')], 6, 'small-heading');
        }
    }, 250);
    $(window).on('resize', debounced);

    /* Removed - This was causing issues with share this feature on iphone */
    /*if (helpers.isTouch()) {
        $('.hero-banner').on('click', function (e) {
            $('.share-hero ul').toggleClass('hidden');
            e.preventDefault();
        });
    }*/

    var heroCarousel = function (selector, button) {
        var $homeCarousel = $(selector),
            $pause = $(button);

        if ($homeCarousel.length) {
            $pause.on('click', function () {
                if ($pause.toggleClass('paused').hasClass('paused')) {
                    $homeCarousel.carousel('pause');
                    $pause.find('span').html('Play');
                }
                else {
                    $homeCarousel.carousel('cycle');
                    $pause.find('span').html('Pause');
                }
                return false;
            });

            /* Used for carousel */
            $homeCarousel.carousel();
        }
    };

    var contentCarousel = function (selector) {
        var $carousel = $(selector);
        if ($carousel.length) {
            $carousel.carousel();
        }
    };

    // carousel for homepage
    heroCarousel('.carousel-home', '.carousel-home .carousel-button');

    // carousel for content page ...
    contentCarousel('.carousel');
});