$(function () {
    /**
    * Use jQuery to get windowHeight ('viewport') and documentHeight ('page')
    * if page > viewport, when scrollTop exceeds viewport height, show '.scrollTop'
    * otherwise hide it
    */
    var showElementScroll = function (elem) {
        var windowHeight = $(window).height(),
            documentHeight = $(document).height(),
            scrollTop = $(window).scrollTop();

        if (documentHeight > windowHeight) {
            if (scrollTop > windowHeight) {
                //elem.show();
                elem.fadeTo('fast', 1);
            }
            else {
                //elem.hide();
                elem.fadeTo('fast', 0);
            }
        }
        else {
            //elem.hide();
            elem.fadeTo('fast', 0);
        }
    }

    /**
    * Scrolling animate to top
    */
    topButton = function () {
        var animatingScroll = function () {
            $('.top-page a').on('click', function () {
                $('html, body').animate({ scrollTop: 0 }, 1000);
                return false;
            });
        };

        animatingScroll();

        var debounced = helpers.debounce(function () {
            showElementScroll($('.top-page a'));
        }, 250);

        $(window).on('resize', debounced);
        $(document).on('scroll', debounced);
    }

    /* Custom top link display */
    topButton();
});