var slideUp;

$(function () {
    slideUp = function ($cta, $selector, buttonText, callback) {
        var $content = $selector,
            height = $content.height() // get element height,
            isMobile = helpers.isMobile();

        $content.css('bottom', '-' + height + 'px');

        $cta.on('click', function (e) {

            // toggle expanded state ...
            if ($cta.attr('style')) {
                $(this).text(buttonText);
                $content.animate({ bottom: '-' + height + 'px' });
                $(this).animate({ bottom: '0px' }, function () {
                    $(this).attr('style', '');
                    $cta.attr('style', '');
                    if (isMobile) { $('section.scene').css('overflow', 'hidden'); }
                });
            } else {
                $(this).animate({ bottom: '' + height + 'px' });
                $content.animate({ bottom: '0px' }, 'slow', function () {
                    if (callback) {
                        callback();
                    }
                    if (isMobile) { $('section.scene').css('overflow', 'visible'); }
                });
                $(this).text('CLOSE');
            }
            e.preventDefault();
        });
    };

    slideUp($('.slide-up-cta-video'), $('.slide-up-content.watch-video'), 'WATCH THE VIDEO');
});