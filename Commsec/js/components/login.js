$(function () {

	var usernamekey = "UserName=";
	var urlkey = "return_url_key=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) 
	{
		var c = $.trim(ca[i]);
	  	if (c.indexOf(usernamekey)==0) 
	  	{
	  		$('#txt-clientId').val('****' + c.substring(usernamekey.length + 4,c.length));
	  		$('#txt-clientId-hidden').val(c.substring(usernamekey.length,c.length));
	  	}else if(c.indexOf(urlkey)==0) 
  		{
	  		var url = c.substring(urlkey.length,c.length);
	  		$( ".take-me-to-link" ).each(function( index ) {
	  			if(url.indexOf($(this).attr('id')) >=0  )
  				{
	  				$(this).addClass('take-me-to-link-hover');
	  				$(this).addClass('take-me-to-link-hover-reserved');
  				}
	  		});
	     	$('#login-form').attr("action", url);
  		}
	}

	//for ie and android we will not be using the default placeholders
	var ua = navigator.userAgent.toLowerCase();
	var useImageForPlaceholder = helpers.isMSIE() || ua.indexOf("android") > -1;
	
	if(useImageForPlaceholder)
	{
		$('#password-field').addClass('ie-placeholder');
	}else
    {
		$('#txt-clientId').attr('placeholder','Client ID');
		$('#password-field').attr('placeholder','Password');
    }

	if( !$('#txt-clientId').val() ) 
	{
        $('#remember-cid-wrapper').hide();
 	  	//ie8hack
 	  	$('#remember-my-clientid').attr('value','off');
 	  	//end ie8hack
 	  	if(useImageForPlaceholder)
 		{
 	  		$('#txt-clientId').addClass('ie-placeholder');
 		}
 	}else
 	{
 	  	$('#remember-cid-wrapper').show();
 	  	$('#remember-my-clientid').prop('checked', true)
 	  							  .attr('value','on');	// ie8hack
 	}
	
	//register events
	
	$("#forgot-password-link").colorbox({iframe:true, width:'750px', height:'600px',escKey: false,  overlayClose: false});

	$(document).mouseup(function(e){ 
	    var popocontainer = $(".popover");
	    if (popocontainer.has(e.target).length === 0){
	    	$('.popover .close').click();
	    }
	});
	
	//forgot un pw
	
	$('#remember-my-clientid-label').popover({
	    placement: 'bottom',
	    html: 'true',
	    trigger: 'trigger',
	    template: '<div class="popover small-popover fade bottom in" ><div class="arrow"></div><h3 class="popover-title" ></h3><div class="popover-content"><div class="popover-content-text"><p></p></div></div></div>'
	});
	
	$(".ForgotUn").popover({
        placement: 'bottom',
        html: 'true'
    }).click(function(e) {
        $('.cid-input-wrapper .popover-content').append('<button  type="button" onclick="$(&quot;.ForgotUn&quot;).popover(&quot;toggle&quot;);" class="close">&times;</button>');
        clickedAway = false;
        isVisible = true;
        e.preventDefault();
    });

	//submit form
	
    $('.dropdown-menu').click( function (e) { 
    	var validCidState = true;
    	var validPwState = true;
    	var isBrowserVersionIE8 = $('#browser-version').hasClass('ie8');
    	
    	if($('#txt-clientId').val().length <=0 )
		{
    		$('.cid-input-wrapper').addClass('input-error');
    		validCidState=false;
		}
    	
    	if($('#password-field').val().length <=0 )
		{
    		$('.pw-input-wrapper').addClass('input-error');
    		validPwState=false;
		}
    	
    	if(validCidState == true && validPwState == true)
    	{
			var redirect_url;
			
			if(e.target.parentElement.id)
			{
				redirect_url = e.target.parentElement.id
			}else
			{
				redirect_url = e.target.id
			}
    		var link = $('#coretx-login-url').val() + redirect_url;
    		$('#login-form').attr("action", link);
    		document.cookie = 'return_url_key=' + link + ';path=/';
    		if($('#remember-my-clientid').is(':checked'))
	  		{
				document.cookie = 'UserName=' + $('#txt-clientId-hidden').val() + ';path=/';
  		    }
    		$("#login-form").submit();
    	}
    	else if(validCidState == true && validPwState == false && !isBrowserVersionIE8)
    	{
    		var errorlabel = $('#error-label');
    		$(errorlabel).text('Please enter your Password.');
    		$('#remember-cid-wrapper').addClass('custom-hidden');
    		$(errorlabel).removeClass('custom-hidden');
    	}
    	else if(validCidState == false && validPwState == true && !isBrowserVersionIE8)
    	{
    		var errorlabel = $('#error-label');
    		$(errorlabel).text('Please enter your Client ID.');
    		$('#remember-cid-wrapper').addClass('custom-hidden');
    		$(errorlabel).removeClass('custom-hidden');
    	}
    	else if(validCidState == false && validPwState == false && !isBrowserVersionIE8)
    	{
    		var errorlabel = $('#error-label');
    		$(errorlabel).text('Please enter your Client ID and Password.');
    		$('#remember-cid-wrapper').addClass('custom-hidden');
    		$(errorlabel).removeClass('custom-hidden');
    	}
    	
    });
    
    $('#btn-login').click( function (e) {
    	var validCidState = true;
    	var validPwState = true;
    	var isBrowserVersionIE8 = $('#browser-version').hasClass('ie8');
    	
    	if($('#txt-clientId').val().length <=0 )
		{
    		$('.cid-input-wrapper').addClass('input-error');
    		validCidState=false;
		}
    	
    	if($('#password-field').val().length <=0 )
		{
    		$('.pw-input-wrapper').addClass('input-error');
    		validPwState=false;
		}
    	
    	if(validCidState == true && validPwState == true)
    	{
    		if($('#remember-my-clientid').is(':checked'))
	  		{
				document.cookie = 'UserName=' + $('#txt-clientId-hidden').val() + ';path=/';
  		    }
    		$("#login-form").submit();
    	}	
		else if(validCidState == true && validPwState == false && !isBrowserVersionIE8)
		{
			var errorlabel = $('#error-label');
			errorlabel.text('Please enter your Password.');
			$('#remember-cid-wrapper').addClass('custom-hidden');
			errorlabel.removeClass('custom-hidden');
		}
		else if(validCidState == false && validPwState == true && !isBrowserVersionIE8)
		{
			var errorlabel = $('#error-label');
			errorlabel.text('Please enter your Client ID.');
			$('#remember-cid-wrapper').addClass('custom-hidden');
			errorlabel.removeClass('custom-hidden');
		}
		else if(validCidState == false && validPwState == false && !isBrowserVersionIE8)
		{
			var errorlabel = $('#error-label');
			errorlabel.text('Please enter your Client ID and Password.');
			$('#remember-cid-wrapper').addClass('custom-hidden');
			errorlabel.removeClass('custom-hidden');
		}
    	return false;
    });
    
    
    //take-me-to events
    
    $('#btnGroupVerticalDrop2').click( function (e) { 
    	
    	var buttonLogin = $('#btn-login');
        if (!$('.btn-group').hasClass('open'))
 		{
        	buttonLogin.text($('#dllHelptext').val());
        	buttonLogin.removeClass('btn-carrot');
   	 	}else
 		{
   	 		buttonLogin.text('LOGIN');
   	 		buttonLogin.addClass('btn-carrot');
   	 		$('.take-me-to-link-hover-reserved').addClass('take-me-to-link-hover');
 		}
   	 	
    });

    $('.take-me-to-link a').focus( function (e) { 
    	var buttonLogin = $('#btn-login');
    	buttonLogin.text($('#dllHelptext').val());
    	buttonLogin.removeClass('btn-carrot');
    	$('.take-me-to-link-hover').removeClass('take-me-to-link-hover');
		$(this).parent().addClass('take-me-to-link-hover');
    });
	
	$('.take-me-to-link a').focusout( function (e) { 
		var buttonLogin = $('#btn-login');
		buttonLogin.text('LOGIN');
		buttonLogin.addClass('btn-carrot');
	    $('.take-me-to-link-hover').removeClass('take-me-to-link-hover');
    });

    $('#btnGroupVerticalDrop2').focusout( function (e) { 
    	var buttonLogin = $('#btn-login');
    	buttonLogin.text('LOGIN');
    	buttonLogin.addClass('btn-carrot');
 		$('.take-me-to-link-hover-reserved').addClass('take-me-to-link-hover');
   });
 
   $('.take-me-to-link').hover(function () {

        $('.take-me-to-link-hover').removeClass('take-me-to-link-hover');
        $(this).addClass('take-me-to-link-hover');
        
	    }, function (){
	        $(this).removeClass('take-me-to-link-hover');
    });

   //checkbox change
   
   $('#remember-my-clientid').click(function(){
		  if(!$(this).is(':checked'))
		  {
			  //ie8 hack
			  $('#remember-my-clientid').attr('value','off');
			  //end ie8 hack
			  
			  if($('#txt-clientId').val().substring(0,4) == '****')
			  {
				  $('#remember-my-clientid-label').popover('hide');
				  $('#txt-clientId').val('');
			  }
			  document.cookie = 'UserName=; expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/';
			  $('#remember-my-clientid-label').popover('hide');
		  }
		  else
		  {
			  //ie8 hack
			  $('#remember-my-clientid').attr('value','on');
			  //end ie8 hack
			  $('#remember-my-clientid-label').popover('show');
			  $('#remember-cid-wrapper .popover-content').append('<button type="button" onclick="$(&quot;#remember-my-clientid-label&quot;).popover(&quot;toggle&quot;);" class="close">&times;</button>');     
		  }
	});

   	//input field changes
   
	$('#txt-clientId').bind("input change propertychange", function() {
		if( !$(this).val() ) 
		{
	          $('#remember-cid-wrapper').hide();
	          $('#remember-my-clientid').prop('checked', false)
	          						    .attr('value','off'); //ie8 hack
			  
	          if(useImageForPlaceholder && !$(this).hasClass('ie-placeholder'))
	          {
	     		//ie hack
	  			$(this).addClass('ie-placeholder');
	  			//end ie hack
	          }
	   	}else
	   	{
	   		//ie hack
	   		$(this).removeClass('ie-placeholder');
			//end ie hack
			
	   		$('.cid-input-wrapper').removeClass('input-error');
	   		$('#remember-cid-wrapper').show();
	   		
	   		if( ! $('.pw-input-wrapper').hasClass('input-error'))
	   		{
	   			$('.error-label').addClass('custom-hidden');
	   			$('#remember-cid-wrapper').removeClass('custom-hidden');   	 	
	   		}else
   			{
	   			$('#error-label').text('Please enter your Password.');
   			}
	   	}
	   	 $('#txt-clientId-hidden').val( $('#txt-clientId').val());
	});
	
	$('#password-field').bind("input change propertychange",function() {
		if( $(this).val() ) {

	   		//ie hack
			$(this).removeClass('ie-placeholder');
			//end ie hack
			
			$('.pw-input-wrapper').removeClass('input-error');
	
			if( ! $('.cid-input-wrapper').hasClass('input-error'))
	   		{      
		          $('.error-label').addClass('custom-hidden');
		          if($('#txt-clientId').val())
	        	  {
		        	  $('#remember-cid-wrapper').removeClass('custom-hidden');
	        	  }
	   		}else
   			{
	   			$('#error-label').text('Please enter your Client ID.');
   			}
	   	}else if(useImageForPlaceholder && !$(this).hasClass('ie-placeholder'))
   		{
   			//ie hack
			$(this).addClass('ie-placeholder');
			//end ie hack
   		}
	});
	
	$('#txt-clientId').focus();
	
	//ie8 hack
	if ($('#browser-version').hasClass('ie8')) {
		$('#remember-cid-wrapper').removeClass('custom-hidden')
								  .addClass('ie8-always-visible')
								  .show();
	}
	//end ie8 hack
	
});