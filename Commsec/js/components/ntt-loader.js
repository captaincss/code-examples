﻿/*
*********************************
    
- Load appropriate JS depending on viewport width
    
*********************************
*/

$(function () {

    //Allow room for scrollbar width
    var breakpointWidth = 820;


    //LOAD DESKTOP
    if ($(window).width() > breakpointWidth && !$('html').hasClass('isIE8')) {

        require(['../shared/runtime', '../shared/chart', 'storyboard', 'animations-ntt', 'slide-up'], function () {
            slideUp($('.slide-up-cta-strategies'), $('.slide-up-content.sample-strategies'), 'What\'s your financial goal?', popupAnimations);
        });

    }
        //LOAD MOBILE
    else {
        require(['../shared/picturefill', 'slide-up'], function () {
            window.picturefill();
            slideUp($('.slide-up-cta-strategies'), $('.slide-up-content.sample-strategies'), 'What\'s your financial goal?');
        });
    }

});