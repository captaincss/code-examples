$(function () {

	// ColorBox resize function
	var resizeTimer;
	function resizeColorBox()
	{
		if (resizeTimer) clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
            if ($('#cboxOverlay').is(':visible') && 750 > $( document ).width() && $('#cboxLoadedContent #t-and-c-popup-body').length > 0) 
            {
                $.colorbox.resize({width:'95%'});

            }else if($('#cboxOverlay').is(':visible') && $('#cboxLoadedContent #t-and-c-popup-body').length > 0)
            {
 					$.colorbox.resize({width:'750px'});
                }
		}, 300);
	}

 	$(".terms-and-conditions-download").each(function( index ) 
        {
            $(this).children('#link-to-t-and-c').colorbox({onOpen:function(){resizeColorBox();},width:'750px',height:'520px', escKey: false, closeButton:false , overlayClose: false,
                    title:  $("#colorbox-title",this).html(),
                    html:  $("#colorbox-html",this).html()
                });
        });

	// Resize ColorBox when resizing window or changing mobile device orientation
	$(window).resize(resizeColorBox);
    if (!window.addEventListener) 
    {
        window.attachEvent("orientationchange", resizeColorBox);
    }
    else 
    {
        window.addEventListener("orientationchange", resizeColorBox, false);
    }
});


