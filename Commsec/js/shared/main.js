﻿/* MAIN.JS - Core initialization file */

/* legacy support ... */
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (elt /*, from*/) {
        var len = this.length >>> 0;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
             ? Math.ceil(from)
             : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}

/* jQuery EXTENSIONS */

jQuery.fn.outerHtml = function () {
    return $($('<div></div>').html(this.clone())).html();
}

/* Utility methods namespaced under 'helpers.' */
var helpers = {

    /*
    borrowed from underscore.js
    called on resize
    prevents methods being excessively called ...
    */
    debounce: function (a, b, c) { var d; return function () { var e = this, f = arguments; clearTimeout(d), d = setTimeout(function () { d = null, c || a.apply(e, f) }, b), c && !d && a.apply(e, f) } },

    /*
    borrowed from underscore.js
    */
    random: function (min, max) {
        if (max == null) {
            max = min;
            min = 0;
        }
        return min + Math.floor(Math.random() * (max - min + 1));
    },

    setUserAgent: function () {
        // set user-agent attribute on html to target via CSS, eg html[data-useragent*='MSIE 10.0']
        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
    },
    getUserAgent: function () {
        return $('html').attr('data-useragent');
    },
    isMSIE: function () {
        if (this.getUserAgent().indexOf('MSIE') > -1) {
            return true;
        }
        else {
            return false;
        }
    },

    /*
    DETECT MOBILE
    assumes .responsive-mobile class will be visible for mobile
    */
    isMobile: function () {
        var $mobileVis = $('.responsive-mobile');
        if ($mobileVis.is(':hidden')) {
            return false;
        }
        return true;
    },

    isTouch: function () {
        var is_touch_device = 'ontouchstart' in document.documentElement;
        return is_touch_device;
    },

    /*
    NB: COOKIE METHODS
    for create, read and erase, see http://www.quirksmode.org/js/cookies.html
    */
    createCookie: function (name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    },
    readCookie: function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    },
    eraseCookie: function (name) {
        createCookie(name, "", -1);
    },
    createCookieCommaSeparated: function (name, value, days) {
        /* 
            create cookie if doesn't exist yet
            if exists, find if this value exists in comma-separated string ...
                if it exists in comma-separated string, do nothing
                if it DOESN'T exist in comma-separated string, append to end
        */
        var myCookie = helpers.readCookie(name);
        if (myCookie) {
            var newValue = myCookie;
            if (!helpers.findMatchinCommaSeparated(myCookie, value)) {
                newValue += ',' + value;
            }
            helpers.createCookie(name, newValue, days);
        }
        else {
            helpers.createCookie(name, value, days);
        }

        function returnNewItem(myArr, myStr)
        {
            for (var i = 0; i < myArr.length; i++) {
                if (myStr != myArr[i]) {
                    return ',' + myStr;
                }
                return '';
            }
        }
    },
    findMatchinCommaSeparated: function (commaSep, myStr)
    {
        /* find if value exists in comma-separated string */
        if (commaSep.match(new RegExp("(?:^|,)" + myStr + "(?:,|$)"))) {
            return true;
        }
        else {
            return false;
        }
    }
};

$(function () {
    reshapeHeadings = function (arrSelector, strLength, className) {
        if (arrSelector.length > 1) {
            for (var i = 0; i < arrSelector.length; i++) {
                DOMElement(arrSelector[i]);
            }
        }
        else {
            DOMElement(arrSelector[0]);
        }

        function DOMElement($selector) {
            // iterate through carousel <h1>s
            $selector.each(
                function () {
                    var $h1 = $(this);
                    var headingText = $h1.html(); // get text
                    var headingSpaces = headingText.split(' '); // split text into words
                    var isBigWord = false;

                    for (var i = 0; i < headingSpaces.length; i++) {
                        // for each word, if we have >= strLength letters ...
                        if (headingSpaces[i].length >= strLength) {
                            isBigWord = true;
                        }
                    }

                    if (isBigWord) {
                        $h1.addClass(className); // add class
                    }
                }
            );
        }
    };

    sticky = function () {
        var offsetTop = 0;

        //If class .no-affix-mob is present, make non-sticky on mobile
        if (helpers.isMobile()) {
            $('.no-affix-mob').attr('data-spy', '');
        }


        $('[data-spy="affix"]').each(function (index, ele) {
            offsetTop = ele.offsetTop - (offsetTop * 2);

            $(this).data('affixOffset', offsetTop);

            $(this).affix({
                offset: {
                    top: offsetTop
                }
            });
        });
    }

    showCollapsible = function () {
        $(document).on('click', '.collapsible-links li', function (e) {
            var $this = $(this);
            if (helpers.isMobile()) {
                // *** only fire this javascript if $mobileVis is visible on the page (should be for MOBILE ONLY)
                $this.find('ul').slideToggle(function () {
                    $this.toggleClass('active');
                });
            }
            e.preventDefault();
        })

        // prevent bubbling up DOM tree (without this, triggers above click feature)
        $(document).on('click', '.collapsible-links ul li ul li a', function (e) {
            e.stopPropagation();
        });
    }

    $('.carousel').carousel().each(function (index, ele) {
        $(ele).find('.carousel-indicators li:first-child').addClass('active');
        $(ele).find('.carousel-inner .item:first-child').addClass('active');
    });

    if (helpers.isTouch() && !helpers.isMobile()) {
        $('body').addClass('touch');

        $('.global-nav .nav').on('click touchstart', 'li', function () {
            $('.global-nav .nav > li').removeClass('active-touch');
            $(this).addClass('active-touch');
        });

        $('.global-nav .nav').on('click touchstart', '.btn-close', function (e) {
            $('.global-nav .nav > li').removeClass('active-touch');
            e.stopPropogation();
            e.preventDefault();
            return false;
        });

        $('section:not(".global")').on('click', function () {
            $('.global-nav .nav > li').removeClass('active-touch');
        });
    }

    $('input, textarea').placeholder();

    sticky(); /* Used for sticky element/navigation functionality */
    showCollapsible(); /* Used by footer and mobile mega-nav (hamburger) */

    helpers.setUserAgent();
});

/* brightcove video resize/reload*/

var experienceMods = [];

$(window).on('orientationchange', function() {
	try{

		if(experienceMods.length > 0) {

			for(var i = 0; i < experienceMods.length; i++) { 
				if (experienceMods[i].experienceMod != null) {
					experienceMods[i].experienceMod.setSize($('#brightcove-container'+experienceMods[i].id).width(), $('#brightcove-container'+experienceMods[i].id).height());
				}
			} 
			
		} else {
				$('.BrightcoveExperience').each(function(index, ele) {
					var parent = $(ele).parent();
					$(ele).remove();
					$(parent).append(ele);
				});
		}
		
	} catch(err) {
		$('.BrightcoveExperience').each(function(index, ele) {
			var parent = $(ele).parent();
			$(ele).remove();
			$(parent).append(ele);
		});
	}
});
