<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.isobar.foxtel.components.imagegallery.ImageGalleryComponent"%>

<c:set var="model" value="<%=new ImageGalleryComponent(pageContext)%>"/>

<c:forEach var="item" items="${model.items}">
<li data-index="${item.index}">
	<div class="item-wrap">
		<div class="item-thumb">
			<picture>
				<!--[if IE 9]><video style="display: none;"><![endif]-->
				<source srcset="${item.imageLarge}" media="(min-width: 1000px)">
				<source srcset="${item.imageMedium}, ${item.imageLarge} 2x" media="(min-width: 641px)">
				<!--[if IE 9]></video><![endif]-->
				<img srcset="${item.imageSmall}, ${item.imageMedium} 2x">
				<svg class="icon icon-loader animation-loader animation-loading"><use xlink:href="#icon-loader"></use></svg>
			</picture>
		</div>
		<div class="gallery-item-copy">
			<div class="cursor-wrap">
				<div class="gallery-item-position">${item.count} of ${model.totalSize}</div>
				<div class="gallery-item-title">${item.title}</div>
				<p class="gallery-item-body">${item.description}</p>
				<small class="gallery-item-credit">${item.credit}</small>
				<a href="#" class="button view">View +</a>
			</div>
			<a href="#" class="button share"><svg class="icon icon-share"><use xlink:href="#icon-share"></use></svg> <span>Share Image</span></a>
		</div>
	</div>
</li>
</c:forEach>