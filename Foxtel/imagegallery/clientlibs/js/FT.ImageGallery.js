/**
 * This module has been wrapped with FT memory mangement logic.
 */
if (FT.exists(FT.COMPONENTS.IMAGEGALLERY)) {
	$(document).ready(function () {

		/**
		 * Code to control the image gallery component
		 * @namespace ImageGallery
		 * @memberof FT
		 * @author Kevin Jarvis
		 * @requires iScroll
		 * @requires JQuery
		 * @requires JQueryMousewheel
		 * @requires Modernizr
		 * @requires Slick
		 */
		FT.ImageGallery = (function () {
			'use strict';

			var galleryContainer = '.component-imagegallery',
				galleryUL = '.gallery ul',
				galleryGridlUL = '.gallery.grid ul',
				numImages = '.num-images',
				iScrollObjects = [],
				DEFAULT_ACTIVE_VIEW = 'default',
				LIST_ACTIVE_VIEW = 'list',
				GRID_ACTIVE_VIEW = 'grid',
				SCROLLING_CLASS = 'is-scrolling',
				MODAL_CLASS,
				MODAL_SCROLLED_CLASS = 'scrolled-down',
				MREC_CLASS = 'mrec',
				LOAD_COMPLETE_CLASS = 'load-complete',
				AT_END_CLASS = 'at-end',
				AT_START_CLASS = 'at-start',
				MOBILE_SCROLL_AMOUNT = 400,
				DESKTOP_SCROLL_AMOUNT = 1150,
				GALLERY_ITEM_TRANSITION_DELAY = 500,
				GALLERY_REMOTE_PATH = 'sliderview-url',
				GALLERY_SECONDARY_REMOTE_PATH = 'gridview-url',
				MODAL_REMOTE_PATH = 'modalview-url',
				UP_DOWN_SWIPE_THRESHOLD = 100;


			/**
			 * Fetch the corresponding gallery data
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {string} fetchURI URI for gallery content
			 * @param  {Number} offset   Offset amount to fetch images from backend
			 * @return {Object} request  Returns the request (jQuery Ajax) object
			 */
			var fetchGalleryData = function (fetchURI, offset) {
				if (offset) {
					fetchURI += '?offset=' + offset;
				}

				var request = $.ajax({
					url: fetchURI,
					dataType: 'html'
				});

				request.fail(function (res) {
					FT.error('Load Failed: ' + res);
					return false;
				});

				return request;
			};


			/**
			 * Fetch the corresponding gallery data (for modal)
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {string} fetchURI URI for modal content
			 * @return {Object} request  Returns the request (jQuery Ajax) object
			 */
			var fetchGalleryModalData = function (fetchURI) {
				var request = $.ajax({
					url: fetchURI,
					dataType: 'html'
				});

				request.fail(function (res) {
					FT.error('Load Failed: ' + res);
					return false;
				});

				return request;
			};


			/**
			 * Set the width of the scroller UL
			 * @memberof FT.ImageGallery
			 * @private
			 * @param {String} galleryUL Bare element of the gallery UL
			 * @return {undefined}
			 */
			var setGalleryWidth = function (galleryUL) {
				var parentComponent = $(galleryUL).parents(galleryContainer);
				if (getActiveView(parentComponent) === DEFAULT_ACTIVE_VIEW) {
					var totalItemsWidth = 0,
						iScrollId = parentComponent.data('iscroll-id');

					$(galleryUL).find('li[data-index]').each(function () {
						totalItemsWidth += $(this).innerWidth();
					});
					$(galleryUL).width(totalItemsWidth + 30);

					refreshSlider(iScrollId);
				}
			};


			/**
			 * When the slider view is active, activate the IScroll element (Includes load more functionality)
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {String} galleryUL Bare element of the gallery UL
			 * @return {undefined}
			 */
			var activateScroller = function (galleryUL) {
				var options = {
						scrollX: true, // enable left/right scrolling
						scrollY: false, // disable Y scrolling
						click: true,
						eventPassthrough: true
					},
					parent = $(galleryUL).parent()[0],
					parentComponent = $(galleryUL).parents(galleryContainer),
					galleryList = $(galleryUL),
					galleryListShared = parentComponent.find('.gallery.list ul, .gallery.slider ul'),
					iScrollId = 't' + new Date().getTime();


				parentComponent.attr('data-iscroll-id', iScrollId);
				setGalleryWidth(galleryUL);

				if (typeof IScroll !== 'undefined') {

					iScrollObjects[iScrollId] = new IScroll(parent, options);

					iScrollObjects[iScrollId].on('scrollStart', function () {
						parentComponent.addClass(SCROLLING_CLASS);
					});

					$(parentComponent).find('.arrow.next').on('click', function (e) {
						//IScroll doesn't trigger scrollStart on calling the scrollBy method, so need to manually add scroling class
						parentComponent.addClass(SCROLLING_CLASS);
						iScrollObjects[iScrollId].scrollBy(-DESKTOP_SCROLL_AMOUNT, 0, 400, IScroll.utils.ease.quadratic);
						e.preventDefault();
					});

					$(parentComponent).find('.arrow.prev').on('click', function (e) {
						//IScroll doesn't trigger scrollStart on calling the scrollBy method, so need to manually add scroling class
						parentComponent.addClass(SCROLLING_CLASS);
						iScrollObjects[iScrollId].scrollBy(DESKTOP_SCROLL_AMOUNT, 0, 400, IScroll.utils.ease.quadratic);
						e.preventDefault();
					});

					// Once reached the end of the scroll, load more (if required)
					iScrollObjects[iScrollId].on('scrollEnd', function () {
						parentComponent.removeClass(SCROLLING_CLASS);

						if ((parentComponent.data('loading') == false || typeof parentComponent.data('loading') === 'undefined') && hasMoreContent(galleryList)) {

							if (this.x < (this.maxScrollX + 200)) {
								var self = this,
									showCount = getShowCount(galleryList);

								parentComponent.data('loading', true);
								var content = fetchGalleryData(parentComponent.data(GALLERY_REMOTE_PATH), showCount);

								FT.log('load more');

								content.done(function (html) {
									addContentToGallery(galleryListShared, html);
									setGalleryWidth(galleryList);
									//self.refresh();
									var scrollAmount = ($(window).width() <= FT.MEDIUM_BREAKPOINT) ? -MOBILE_SCROLL_AMOUNT : -DESKTOP_SCROLL_AMOUNT;
									iScrollObjects[iScrollId].scrollBy(scrollAmount, 0, 800, IScroll.utils.ease.quadratic);
									setTimeout(function () {
										//initializePicturefillImages();
										picturefill();
										bindImageLoadEvent();
									}, 800);
									parentComponent.data('loading', false);
								});


							}
						} else {
							// Detect if at end of scroll (and if no more content to load), and add class (for styling of RHS arrow)
							parentComponent.addClass(LOAD_COMPLETE_CLASS);
							if (this.x < (this.maxScrollX + 200)) {
								parentComponent.addClass(AT_END_CLASS);
							} else {
								parentComponent.removeClass(AT_END_CLASS);
							}
						}

						// Detect if at start of scroll position, add class (for styling of LHS arrow)
						if (this.x > -200) {
							parentComponent.addClass(AT_START_CLASS);
						} else {
							parentComponent.removeClass(AT_START_CLASS);
						}
					});
				}
			};


			/**
			 * Set the active view for the gallery
			 * @memberof FT.ImageGallery
			 * @private
			 * @param {String} galleryEle Container gallery element
			 * @param {String} state      Gallery view state
			 * @return {undefined}
			 */
			var setActiveView = function (galleryEle, state) {
				var displayView = state || DEFAULT_ACTIVE_VIEW,
					gallery = $(galleryEle).find(galleryUL)[0];

				$(galleryEle).attr('data-active-view', displayView);

				switch (displayView) {
				case DEFAULT_ACTIVE_VIEW:
					// When images are loaded in slider, add loaded class
					bindImageLoadEvent();
					setGalleryWidth(gallery);
					$(galleryEle).find('.pill-tabs .tab').removeClass('active');
					$(galleryEle).find('.pill-tabs .tab:first-child').addClass('active');
					break;
				case LIST_ACTIVE_VIEW:
					updateShowCount(galleryEle);
					break;
				case GRID_ACTIVE_VIEW:
					updateShowCount(galleryEle);
					break;
				}

			};


			/**
			 * Return the current active view of the gallery
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {String} galleryEle Gallery element to get active view of
			 * @return {String}            Gallery active view
			 */
			var getActiveView = function (galleryEle) {
				return $(galleryEle).attr('data-active-view');
			};


			/**
			 * Update text label with currently visible count of thumbs
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {String} galleryEle Gallery Element to get count from
			 * @return {undefined}
			 */
			var updateShowCount = function (galleryEle) {
				var count = $(galleryEle).find('.count')[0],
					itemsVisible;

				if (getActiveView(galleryEle) === GRID_ACTIVE_VIEW) {
					itemsVisible = $(galleryEle).find(galleryGridlUL + ' li').length;
				} else {
					itemsVisible = $(galleryEle).find('.gallery.list li:not(.mrec)').length;
				}

				$(count).text(itemsVisible);
			};


			/**
			 * Return how many items are visible
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {String} galleryEle Gallery element UL
			 * @return {Number}            Count of how many items are visible
			 */
			var getShowCount = function (galleryUL) {
				return $(galleryUL).find('li').length;
			};


			/**
			 * Use slick slider to navigate to the appropriate index
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {Object} slickObj Appropriate slick object
			 * @param  {Number} imgIndex Index of image to slide to
			 * @return {undefined}
			 */
			var navigateToIndex = function (slickObj, imgIndex) {
				slickObj.slickGoTo(imgIndex);
			};


			/**
			 * Return whether or not there is more content to load
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {Element}  galleryEle Gallery element (container)
			 * @return {Boolean}            Return true/false depending on if more items to load
			 * @return {undefined}
			 */
			var hasMoreContent = function (galleryUL) {
				var galleryItemCount = $(galleryUL).parents(galleryContainer).data('gallery-count'),
					galleryActualCount = $(galleryUL).find('li[data-index]').length;

				return galleryActualCount < galleryItemCount;
			};


			/**
			 * Append fetch data(markup) to the gallery
			 * @memberof FT.ImageGallery
			 * @private
			 * @param {Element} galleryUL         Gallery to append the content to
			 * @param {String} additionalContent Markup to add to the gallery
			 * @return {undefined}
			 */
			var addContentToGallery = function (galleryUL, additionalContent) {
				$(galleryUL).append(additionalContent);
				updateShowCount($(galleryUL).parents(galleryContainer)[0]);
			};


			/**
			 * Animate text in modal element
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {Boolean} animateIn Whether to animate text up/down
			 * @return {undefined}
			 */
			var animateTextIn = function (animateIn) {
				if (animateIn) {
					$(MODAL_CLASS).addClass(MODAL_SCROLLED_CLASS);
				} else {
					$(MODAL_CLASS).removeClass(MODAL_SCROLLED_CLASS);
				}
			};


			/**
			 * Set the count of the number of images available
			 * @memberof FT.ImageGallery
			 * @private
			 * @param {String} galleryEle Gallery element to set image count
			 * @return {undefined}
			 */
			var setTotalImages = function (galleryEle) {
				var total = $(galleryEle).data('gallery-count');
				$(galleryEle).find(numImages).text(total);
			};


			/**
			 * Show the modal when clicking on a thumb
			 * @memberof FT.ImageGallery
			 * @private
			 * @return {undefined}
			 */
			var showModalImage = function () {
				FT.Modal.showModal();
			};


			/**
			 * Display appropriate image on page load (deep linking)
			 * @memberof FT.ImageGallery
			 * @private
			 * @return {undefined}
			 */
			var displayImageOnLoad = function () {
				var hash = window.location.hash;
				if (hash && hash !== "") {
					hash = hash.replace('#', '');
					var params = hash.split('&'),
						galleryId = params[0].split('=')[1],
						imageIndex = params[1].split('=')[1];

					loadAndShowModalContent(galleryId, imageIndex);
				}
			};


			/**
			 * Update the URI to reflect what image we are displaying
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {String} galleryId  galleryElementID (in modal)
			 * @param  {Number} imageIndex image index to display
			 * @return {undefined}
			 */
			var updateUriHash = function (galleryId, imageIndex) {
				if (Modernizr.history) {
					if (galleryId) {
						var hash = '#galleryId=' + galleryId + '&imageIndex=' + imageIndex;
						history.replaceState({}, '', hash);
					} else {
						history.replaceState({}, '', window.location.pathname);
					}
				}
			};


			/**
			 * Lazily load picture/picturefill images, replaces span with picture element
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {String} containerEle Container element containing the span to be replaced
			 * @return {undefined}
			 */
			var lazyLoad = function (containerEle) {
				// Lazy load picturefill images - http://codepen.io/Chmood/pen/CHlGD
				if (containerEle.length) {
					FT.log('lazy load');

					var content = $(containerEle).html(),
						$pic = $('<picture class="showcase">' + content + '</picture>');

					$(containerEle).after($pic);

					$pic.find('img').each(function () {
						$(this).on('load', function () {
							$(this).parents('.showcase').addClass('loaded');
						});
					});
					$(containerEle).remove();
				}
			};


			/**
			 * If the modal content has been loaded, show the modal with the appropriate image,
			 * otherwise, load the modal content and then display the appropriate image
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {String} galleryId  Gallery id to fetch the content for (check if exists)
			 * @param  {type} imageIndex Image index to display
			 * @return {undefined}
			 */
			var loadAndShowModalContent = function (galleryId, imageIndex) {
				var galleryEle = $(MODAL_CLASS + ' #' + galleryId),
					parentComponent = $(galleryContainer + '[data-gallery-id="' + galleryId + '"]');

				// If we can find the appropriate gallery (else do nothing, dont show modal)
				if (parentComponent.length) {

					$(MODAL_CLASS + ' .slider').removeClass('active');

					if (galleryEle.length) {
						galleryEle.addClass('active');
						navigateToIndex(galleryEle, imageIndex);
						showModalImage();
					} else {
						var content = fetchGalleryModalData(parentComponent.data(MODAL_REMOTE_PATH));
						// If was able to retreive gallery content...
						content.done(function (html) {
							galleryEle = $(html).slick({
								//cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
								speed: 400,
								draggable: false,
								prevArrow: '<button type="button" class="slick-prev"><svg class="icon icon-arrow-left"><use xlink:href="#icon-arrow-left"></use></svg></button>',
								nextArrow: '<button type="button" class="slick-next"><svg class="icon icon-arrow-right"><use xlink:href="#icon-arrow-right"></use></svg></button>',
								onAfterChange: function () {
									lazyLoad(galleryEle.find('.slick-active span.showcase'));
									lazyLoad(galleryEle.find('.slick-active').next().find('span.showcase'));
									lazyLoad(galleryEle.find('.slick-active').prev().find('span.showcase'));

									FT.log('loadAndShowModalContent');
									//initializePicturefillImages();
									picturefill({
										reevaluate: true
									});

									// Update URI hash when changing slides
									var index = galleryEle.find('.slick-active').attr('index');
									updateUriHash(galleryId, index);
								},
								onBeforeChange: function () {
									$(MODAL_CLASS).removeClass(MODAL_SCROLLED_CLASS);
								}
							});

							FT.Modal.addContent(galleryEle);
							navigateToIndex(galleryEle, imageIndex);
							showModalImage();
						});
					}

				}

			};


			/**
			 * This is used within the slider, to add a load event handler to images,
			 * which in effect sets the width of the slider element
			 * @memberof FT.ImageGallery
			 * @private
			 * @return {undefined}
			 */
			var bindImageLoadEvent = function () {
				$(galleryContainer).find(galleryUL + ' picture img').each(function () {
					var parentComponent = $(this).parents(galleryContainer),
						iScrollId = parentComponent.data('iscroll-id'),
						galleryList = parentComponent.find(galleryUL)[0];

					// If the image hasn't already loaded (could be cached)
					if (!this.complete) {
						$(this).on('load', function () {
							$(this).parents('li').addClass('loaded');
							setTimeout(function () {
								setGalleryWidth(galleryList);
							}, GALLERY_ITEM_TRANSITION_DELAY);
							//FT.log('loaded');
						});
					} else {
						$(this).parents('li').addClass('loaded');
						setTimeout(function () {
							setGalleryWidth(galleryList);
						}, GALLERY_ITEM_TRANSITION_DELAY);
						//FT.log('loaded CACHED');
					}
					/*setGalleryWidth(galleryList);
					refreshSlider(iScrollId);*/
				});
			};


			/**
			 * Call the iSliders internal refresh method
			 * @memberof FT.ImageGallery
			 * @private
			 * @param  {String} sliderId SliderId to reference (as they are stored in an array) to refresh
			 * @return {undefined}
			 */
			var refreshSlider = function (sliderId) {
				if (iScrollObjects[sliderId]) {
					setTimeout(function () {
						iScrollObjects[sliderId].refresh();
					}, 200);
				}
			};


			/**
			 * If at the small or below breakpoint,
			 * modify img sources to display small images (prevent double image load)
			 * @memberof FT.ImageGallery
			 * @private
			 * @return {undefined}
			 */
			var initializePicturefillImages = function () {

				if ($(window).width() <= FT.MEDIUM_BREAKPOINT) {
					$('picture img').each(function () {
						$(this).attr('srcset', $(this).attr('data-srcset'));
					});
					FT.log('initialize pf done');
				}

			};


			/**
			 * Setup click events on elements
			 * @memberof FT.ImageGallery
			 * @private
			 * @return {undefined}
			 */
			var setupEvents = function () {

				// To stop smaller size images being downloaded as well
				//initializePicturefillImages();

				picturefill({
					reevaluate: true
				});

				// On page load -> if hash contains gallery and image index, display modal
				displayImageOnLoad();

				// Setup click events for tabs (Change view)
				$(galleryContainer).each(function () {
					var self = $(this)[0],
						gallery = $(self).find(galleryUL)[0];

					setActiveView(self, $(this).data('active-view'));
					activateScroller(gallery);
					setTotalImages(self);

					$(this).find('.tabs li').on('click', function (e) {
						var displayView = $(this).data('display-view');
						setActiveView(self, displayView);
					});
				});

				// Setup click events for load more (button click)
				$(galleryContainer + ' .load-more').on('click', function (e) {
					var parentComponent = $(this).parents(galleryContainer),
						galleryList,
						galleryListShared = parentComponent.find('.gallery.list ul, .gallery.slider ul'),
						self = $(this),
						activeView = parentComponent.attr('data-active-view'),
						grid = false;

					if (activeView === GRID_ACTIVE_VIEW) {
						galleryList = parentComponent.find(galleryGridlUL)[0];
						grid = true;
					} else {
						galleryList = parentComponent.find(galleryUL)[1];
					}
					//galleryList.detach('.load-more');
					// TODO: Refactor out to make ajax call
					if (hasMoreContent(galleryList)) {
						var path = grid ? GALLERY_SECONDARY_REMOTE_PATH : GALLERY_REMOTE_PATH,
							showCount = getShowCount(galleryList),
							content = fetchGalleryData(parentComponent.data(path), showCount);

						content.done(function (html) {
							if (grid) {
								addContentToGallery(galleryList, html);
							} else {
								addContentToGallery(galleryListShared, html);
							}

							//initializePicturefillImages();
							picturefill();
							bindImageLoadEvent();

							// Hide load more button if no more content
							if (!hasMoreContent(galleryList)) {
								FT.log($(galleryList));
								$(galleryList).parent().addClass(LOAD_COMPLETE_CLASS);
							}
						});
					}

					e.preventDefault();
				});

				/**
				 * On clicking the close button, set active view to default, and position scroll to top of gallery
				 * @param  {Event} e Click event
				 */
				$(galleryContainer + ' .button.close').on('click', function (e) {
					var galleryEle = $(this).parents(galleryContainer)[0],
						galleryTop = $(galleryEle).offset().top;

					setActiveView(galleryEle);
					$(window).scrollTop(galleryTop);
					e.preventDefault();
				});

				// Prevent opening modal if I click on the share button
				$(galleryContainer).on('click', '.button.share', function (e) {
					e.stopPropagation();
				});

				// Setup click event to show modal
				$(galleryContainer).on('click', '.gallery.slider ul li, .gallery.list .item-thumb, .gallery.grid ul li', function (e) {
					var fetchURI = $(e.delegateTarget).data(MODAL_REMOTE_PATH),
						galleryId = $(e.delegateTarget).data('gallery-id'),
						imageIndex = $(this).index();

					if ($(this).hasClass('item-thumb')) {
						imageIndex = $(this).parents('li').index();
					}

					// If is an mrec, proceed with link
					if ($(this).hasClass(MREC_CLASS)) {
						return true;
					}

					loadAndShowModalContent(galleryId, imageIndex);
					updateUriHash(galleryId, imageIndex);

					e.preventDefault();
				});

				$(MODAL_CLASS + ' .close').on('click', function () {
					updateUriHash();
					FT.log('close');
				});

				/**
				 * Used to move text/copy up/down in lightbox (based on mousewheel)
				 * NOTE: Relies on jquery.mousewheel for cross-browser compatibility
				 * @param  {Event} e MouseWheel even
				 */
				$(MODAL_CLASS).on('mousewheel', function (e) {
					if (e.deltaY > 0) {
						/* up scroll */
						animateTextIn(false);
					} else {
						/* down scroll */
						animateTextIn(true);
					}
				});

				/**
				 * Used to move text/copy up/down in lightbox (based on touch/swipe)
				 * @param  {Event} e MouseWheel even
				 */
				if (Modernizr.touch) {
					var yDown = null;

					$(MODAL_CLASS).on('touchstart', function (e) {
						yDown = e.originalEvent.touches[0].clientY;
					}).on('touchmove', function (e) {
						// Need the below as android has a bug where touchmove only gets fired
						// once if this isn't included - http://uihacker.blogspot.com.au/2011/01/android-touchmove-event-bug.html
						if (navigator.userAgent.match(/Android/i)) {
							e.preventDefault();
						}
						if (!yDown) {
							return;
						}
						var yUp = e.originalEvent.touches[0].clientY,
							yDiff = yDown - yUp;

						// If you have swiped more or less than threshold pixels up or down,
						// then animate text
						if (yDiff <= -UP_DOWN_SWIPE_THRESHOLD || yDiff >= UP_DOWN_SWIPE_THRESHOLD) {
							if (yDiff > 0) {
								/* up swipe */
								animateTextIn(true);
							} else {
								/* down swipe */
								animateTextIn(false);
							}
						}
					});
				}

				// If the browser moves to beneath small breakpoint, initialize small picturefill images
				/*$(window).on('resize', Foundation.utils.throttle(function () {
					FT.log('resize');
					initializePicturefillImages();
				}, 500));*/

			};

			/**
			 * Standard entry point for JS modules. The constructor is always the last function in a module.
			 * @memberof FT.ImageGallery
			 * @private
			 * @return {undefined}
			 */
			var constructor = (function () {
				MODAL_CLASS = FT.Modal.getClassName();
				setupEvents();
			})();

			return {};
		})();
	});
}