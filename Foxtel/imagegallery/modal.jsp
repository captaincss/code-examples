<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.isobar.foxtel.components.imagegallery.ImageGalleryComponent"%>

<c:set var="model" value="<%=new ImageGalleryComponent(pageContext)%>"/>

<div id="gallery-${model.id}" class="slider active">
	<c:forEach var="item" varStatus="itemStatus" items="${model.items}">
	<div class="content">
		<span class="showcase">
			<!--[if IE 9]><video style="display: none;"><![endif]-->
			<source srcset="${item.imageFull}" media="(min-width: 1000px)">
			<source srcset="${item.imageMedium}, ${item.imageLarge} 2x, ${item.imageFull} 3x" media="(min-width: 641px)">
			<!--[if IE 9]></video><![endif]-->
			<img srcset="${item.imageSmall}, ${item.imageMedium} 2x">
			<svg class="icon icon-loader animation-loader animation-loading"><use xlink:href="#icon-loader"></use></svg>
		</span>
		<div class="body-wrap">
			<div class="body">
				<div class="inner-wrap">
					<div class="copy">
						<div class="item-count">${item.count} of ${model.totalSize}</div>
						<div class="item-title">${item.title}</div>
						<div class="item-description">${item.description}</div>
						<div class="item-credit">${item.credit}</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#" class="button dark share"><svg class="icon icon-share"><use xlink:href="#icon-share"></use></svg></a>
	</div>
	</c:forEach>
</div>