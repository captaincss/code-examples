<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.isobar.foxtel.components.imagegallery.ImageGalleryComponent"%>

<c:set var="model" value="<%=new ImageGalleryComponent(pageContext)%>"/>

<c:forEach var="item" items="${model.items}">
<li data-index="${item.index}">
	<div class="item-wrap">
		<div class="item-thumb">
			<img src="${item.imageGrid}">
		</div>
	</div>
</li>
</c:forEach>