<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.isobar.foxtel.components.imagegallery.ImageGalleryComponent"%>
<c:set var="model" value="<%=new ImageGalleryComponent(pageContext)%>" />
<section class="container-single-column accent-primary overflow-visible">
	<div class="container-content">
		<div class="component-imagegallery at-start" data-gallery-id="gallery-${model.id}" data-gallery-count="${model.totalSize}" data-active-view="${model.defaultView}"
		data-gridview-url="${model.gridViewUrl}" data-sliderview-url="${model.sliderViewUrl}" data-modalview-url="${model.modalViewUrl}">

			<div class="container-wrapper">
				<div class="gallery-copy">
					<c:if test="${not empty model.title}">
			    	<span class="heading-gallerytitle">${model.title}</span>
			    	</c:if>
			    	<c:if test="${not empty model.intro}">
			    	<p class="galleryintro">${model.intro}</p>
			    	</c:if>
			    	<div class="gallery-sort">
			    		<c:if test="${model.showToggle}">
			        	<ul class="pill-tabs tabs">
			        		<c:if test="${model.sliderViewEnabled}">
							<li class="tab pill-tab info-tab ${model.sliderViewActive}" data-toggle-class="info-content" data-display-view="default">
								<svg class="icon icon-slider"><use xlink:href="#icon-slider"></use></svg>
							</li>
							</c:if>
							<c:if test="${model.listViewEnabled}">
							<li class="tab pill-tab related-tab ${model.listViewActive}" data-toggle-class="related-content" data-display-view="list">
								<svg class="icon icon-list"><use xlink:href="#icon-list"></use></svg>
							</li>
							</c:if>
							<c:if test="${model.gridViewEnabled}">
							<li class="tab pill-tab related-tab ${model.gridViewActive}" data-toggle-class="related-content" data-display-view="grid">
								<svg class="icon icon-grid"><use xlink:href="#icon-grid"></use></svg>
							</li>
							</c:if>
						</ul>
						</c:if>
						<div class="gallery-count">
							<p><span class="num-images">${model.totalSize}</span> images</p>
						</div>
					</div>
				</div>
			</div>

			<div class="gallery slider">
				<div class="container-wrapper">
					<ul>
						<c:forEach var="item" items="${model.items}">
						<li data-index="${item.index}">
							<div class="item-wrap">
								<div class="item-thumb">
									<picture>
										<!--[if IE 9]><video style="display: none;"><![endif]-->
										<source srcset="${item.imageLarge}" media="(min-width: 1000px)">
										<source srcset="${item.imageMedium}, ${item.imageLarge} 2x" media="(min-width: 641px)">
										<!--[if IE 9]></video><![endif]-->
										<img srcset="${item.imageSmall}, ${item.imageMedium} 2x">
										<svg class="icon icon-loader animation-loader animation-loading"><use xlink:href="#icon-loader"></use></svg>
									</picture>
								</div>
								<div class="gallery-item-copy">
									<div class="cursor-wrap">
										<div class="gallery-item-position">${item.count} of ${model.totalSize}</div>
										<div class="gallery-item-title">${item.title}</div>
										<p class="gallery-item-body">${item.description}</p>
										<small class="gallery-item-credit">${item.credit}</small>
										<a href="#" class="button view">View +</a>
									</div>
									<a href="#" class="button share"><svg class="icon icon-share"><use xlink:href="#icon-share"></use></svg> <span>Share Image</span></a>
								</div>
							</div>
						</li>
						</c:forEach>
					</ul>
				</div>
				<div class="arrows">
					<a href="#" class="arrow prev">
						<div class="button-wrap circled">
							<svg class="icon icon-chevron-left"><use xlink:href="#icon-chevron-left"></use></svg>
						</div>
					</a>
					<a href="#" class="arrow next">
						<div class="button-wrap circled">
							<svg class="icon icon-chevron-right"><use xlink:href="#icon-chevron-right"></use></svg>
						</div>
					</a>
				</div>
			</div>

			<div class="container-wrapper">
				<div class="gallery list">
					<ul>
						<c:forEach var="item" items="${model.items}">
						<li data-index="${item.index}">
							<div class="item-wrap">
								<div class="item-thumb">
									<picture>
										<!--[if IE 9]><video style="display: none;"><![endif]-->
										<source srcset="${item.imageLarge}" media="(min-width: 1000px)">
										<source srcset="${item.imageMedium}, ${item.imageLarge} 2x" media="(min-width: 641px)">
										<!--[if IE 9]></video><![endif]-->
										<img srcset="${item.imageSmall}, ${item.imageMedium} 2x">
										<svg class="icon icon-loader animation-loader animation-loading"><use xlink:href="#icon-loader"></use></svg>
									</picture>
								</div>
								<div class="gallery-item-copy">
									<div class="cursor-wrap">
										<div class="gallery-item-position">${item.count} of ${model.totalSize}</div>
										<div class="gallery-item-title">${item.title}</div>
										<p class="gallery-item-body">${item.description}</p>
										<small class="gallery-item-credit">${item.credit}</small>
										<a href="#" class="button view">View +</a>
									</div>
									<a href="#" class="button share"><svg class="icon icon-share"><use xlink:href="#icon-share"></use></svg> <span>Share Image</span></a>
								</div>
							</div>
						</li>
						</c:forEach>
					</ul>
				</div>
				<div class="gallery grid">
					<ul>
						<c:forEach var="item" items="${model.items}">
						<li data-index="${item.index}">
							<div class="item-wrap">
								<div class="item-thumb">
									<img src="${item.imageGrid}">
								</div>
							</div>
						</li>
						</c:forEach>
					</ul>
				</div>

				<div class="peripheral-links">
					<div class="load-more">
						<c:if test="${model.totalSize > 12}">
						<a href="#" class="button-arrow down-motion">
							<span>Load more images</span>
							<svg class="icon icon-chevron-down"><use xlink:href="#icon-chevron-down"></use></svg>
						</a>
						</c:if>
						<div class="show-count">Currently Showing <span class="count">${model.itemsSize}</span> of <span class="num-images">${model.totalSize}</span></div>
					</div>
					<div class="action-bar">
						<a href="#" class="button share"><svg class="icon icon-share"><use xlink:href="#icon-share"></use></svg> Share Gallery</a>
						<a href="#" class="button close"><svg class="icon icon-close"><use xlink:href="#icon-close"></use></svg> Close <span>Gallery</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
