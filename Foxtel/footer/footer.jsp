<!-- FNA Footer -->

<%@include file="/libs/foundation/global.jsp"%>
<%@page session="false" %>
<%@page import="com.isobar.foxtel.components.footer.Footer"%>

<%
	Footer footer = new Footer(pageContext);
	pageContext.setAttribute("footer", footer);
%>

<footer class="footer-main">

    <nav>
        <div class="row">
            <ul class="social-links">
		        <c:if test="${not empty footer.channelLogoRef}">
	                <li class="main-logo">
		      			<a href="${footer.channelLogoLink}"><img src="${footer.channelLogoRef}" alt="${footer.channelLogoAlt}"/></a>
	                </li>
		      	</c:if>
                <c:if test="${not empty footer.facebookUrl}">
                     <li class="social-item"><a href="${footer.facebookUrl}" class="circled-icon" target="_blank"><svg class="icon icon-facebook"><use xlink:href="#icon-facebook"></use></svg></a></li>
                </c:if>
                <c:if test="${not empty footer.pinterestUrl}">
                     <li class="social-item"><a href="${footer.pinterestUrl}" class="circled-icon" target="_blank"><svg class="icon icon-pinterest"><use xlink:href="#icon-pinterest"></use></svg></a></li>
                </c:if>
                <c:if test="${not empty footer.twitterUrl}">
                      <li class="social-item"><a href="${footer.twitterUrl}" class="circled-icon" target="_blank"><svg class="icon icon-twitter"><use xlink:href="#icon-twitter"></use></svg></a></li>
                </c:if>
                <c:if test="${not empty footer.googlePlusUrl}">
                     <li class="social-item"><a href="${footer.googlePlusUrl}" class="circled-icon" target="_blank"><svg class="icon icon-googleplus"><use xlink:href="#icon-googleplus"></use></svg></a></li>
                </c:if>
              	<c:if test="${not empty footer.instagramUrl}">
    				 <li class="social-item"><a href="${footer.instagramUrl}" class="circled-icon" target="_blank"><svg class="icon icon-instagram"><use xlink:href="#icon-instagram"></use></svg></a></li>
    			</c:if>
            </ul>
            <c:if test="${not empty footer.footerList}">
                <div class="footer-links">
                    <c:forEach var="item" items="${footer.footerList}">
                        <div class="footer-item"><a href="${item.targetPath}">${item.title}</a></div>
                    </c:forEach>
                </div>
            </c:if>
        </div>
    </nav>
    <div class="footer-bottom">
        <div class="row">
            <div class="footer-logos">
            	<c:if test="${not empty footer.brandingLogoRef}">
                	<a href="${footer.brandingLogoLink}"><img class="foxtel-logo" src="${footer.brandingLogoRef}" alt="${footer.brandingLogoAlt}"/></a>
            	</c:if>
            </div>
            <div class="footer-copyright">&copy; ${footer.disclaimerText} ${footer.currentYear}</div>
        </div>
    </div>
</footer>