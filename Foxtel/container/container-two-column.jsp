<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.isobar.foxtel.components.container.Container"%>
<c:set var="model" value="<%=new Container(pageContext)%>" />
<section class="${model.additionalCSSclasses} container-two-column">
	<div class="container-wrapper">
		<div class="container-content">
		    <div class="container-content-left">
		    	<cq:include path="parLeft" resourceType="foundation/components/parsys"/>
		    </div>

			<div class="container-content-right">
				<cq:include path="parRight" resourceType="foundation/components/parsys"/>
			</div>
		</div>
	</div>
</section>