<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.isobar.foxtel.components.container.Container"%>
<c:set var="model" value="<%=new Container(pageContext)%>" />
<section class="${model.additionalCSSclasses} container-fixed-sidebar">
	<div class="container-wrapper">
		<div class="container-content">
		    <div class="container-content-main">
		    	<cq:include path="par" resourceType="foundation/components/parsys"/>
		    </div>

			<div class="container-content-sidebar">
				<cq:include path="parSidebar" resourceType="foundation/components/parsys"/>
			</div>
		</div>
	</div>
</section>