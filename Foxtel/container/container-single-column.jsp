<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.isobar.foxtel.components.container.Container"%>
<c:set var="model" value="<%=new Container(pageContext)%>" />
<section class="${model.additionalCSSclasses} container-single-column">
	<div class="container-wrapper">
		<div class="container-content">
			<cq:include path="par" resourceType="foundation/components/parsys"/>
		</div>
	</div>
</section>