/**
 * This module has been wrapped with FT memory mangement logic.
 */
if (FT.exists(FT.COMPONENTS.NAVIGATION)) {
	$(document).ready(function () {

		/**
		 * Code to control the primary and secondary levels of navigation
		 * @namespace Navigation
		 * @memberof FT
		 * @author Adam Burns, Kevin Jarvis
		 * @requires iScroll
		 * @requires JQuery
		 * @requires JQueryCookie
		 * @requires Modernizr
		 */
		FT.Navigation = (function () {
			'use strict';

			//TODO: Refactor this JS to be less clunky
			var SCROLL_AMOUNT = 200,
				globalnavClass = '.globalnavigation',
				hamburgerClass = '.menu-button',
				primaryNavClass = '.primary-nav',
				scrollableClass = '.scrollable',
				wrapperClass = '.nav-wrapper',
				navbarClass = '.nav-bar',
				showMoreClass = '.show-more',
				canScrollClass = 'can-scroll',
				innerNav;


			/**
			 * Do initial navigation setup, setup iScroll
			 * @memberof FT.Navigation
			 * @private
			 * @return {undefined}
			 */
			var setupNav = function () {

				$(navbarClass + scrollableClass).each(function () {
					var totalItemsWidth = 0;
					$(this).find('> li').each(function () {
						totalItemsWidth += $(this).innerWidth();
					});
					$(this).width(totalItemsWidth + 200);
				});

				var options = {
					scrollX: true,
					scrollY: false,
					eventPassthrough: true,
					preventDefault: false
				};

				// Need to check if is a touch device and allow click event for IScroll..
				if (Modernizr.touch) {
					//options.preventDefault = false;
					//options.click = true; // required to allow click events on mobile devices
				}

				// Initialize IScroll for each nav bar
				if (typeof IScroll !== 'undefined') {

					$(navbarClass + scrollableClass).each(function () {
						var parentEle = $(this).parent(wrapperClass)[0];
						var navbar = new IScroll(parentEle, options);

						//if (!$(this).find('li:first-child').hasClass('active'))
						//navbar.scrollToElement('.active');
					});
				}

				//Add loaded class to prevent flicker on IE
				$(globalnavClass).addClass('loaded');
			};

			/**
			 * If you can scroll, add scroll class (for cursor symbol)
			 */
			var addCanScrollClass = function () {
				if (!Modernizr.touch) {
					$(navbarClass + scrollableClass).each(function () {
						if ($(this).innerWidth() > $(this).parent(wrapperClass).innerWidth()) {
							$(this).addClass(canScrollClass);
						} else {
							$(this).removeClass(canScrollClass);
						}
					});
				}
			};


			/**
			 * Global nav specific - slide in on load
			 * @memberof FT.Navigation
			 * @private
			 * @return {undefined}
			 */
			var addLoadedClass = function () {
				if ($.cookie('animatenav') === undefined) {
					var totalItemsWidth = 0;

					$(globalnavClass + ' ' + navbarClass + ' li').each(function () {
						totalItemsWidth += $(this).innerWidth();
					});

					if (totalItemsWidth > $(globalnavClass + ' ' + wrapperClass).innerWidth()) {
						$(globalnavClass + ' ' + navbarClass).addClass('animate-in');
					}

					$.cookie('animatenav', 'done');
				}
			};


			/**
			 * Setup click events on navigation elements
			 * @memberof FT.Navigation
			 * @private
			 * @return {undefined}
			 */
			var setupEvents = function () {
				var $expandedNav = $('.expanded-nav');

				$(globalnavClass + ' ' + hamburgerClass).on('click', function (e) {
					$(globalnavClass).toggleClass('open');
					$(this).toggleClass('open');
					$expandedNav.toggleClass('open');

					if (typeof IScroll !== 'undefined' && FT.isSmallBreakpoint()) {
						// add the scroller to the overlay if it doesnt yet exist
						if (typeof innerNav === 'undefined') {
							// init the scroller
							// this needs to occur in a timeout, as iScroll can't calculate the correct height of the
							// nav if its hidden

							var options = {
								scrollX: false,
								scrollY: true
							};

							// Need to check if is a touch device and allow click event for IScroll..
							if (Modernizr.touch) {
								options.click = true; // required to allow click events on mobile devices
							}

							setTimeout(function () {
								$('.nav-scroller').wrap('<div class="nav-scroller-wrapper"></div>');

								innerNav = new IScroll($('.nav-scroller-wrapper')[0], options);
							}, 500);

						} else {
							// scroller already exists, and the we're now closing the nav. scroll to the top
							setTimeout(function () {
								innerNav.scrollTo(0, 0, true);
							}, 1000);
						}
					}
				});

				$('.show-more').on('click', function (e) {
					$(this).toggleClass('active');
					e.preventDefault();
				});

				$(window).on('resize', FT.debounce(function () {
					addCanScrollClass();
				}));
			};


			/**
			 * Standard entry point for JS modules. The constructor is always the last function in a module.
			 * @memberof FT.Navigation
			 * @private
			 * @return {undefined}
			 */
			var constructor = (function () {
				setupNav();
				addLoadedClass();
				addCanScrollClass();
				setupEvents();
			})();


			return {};
		})();
	});
}