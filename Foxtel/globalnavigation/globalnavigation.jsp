<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page    import="com.isobar.foxtel.components.navigation.GlobalNavigationComponent"%>

<c:set var="model" value="<%=new GlobalNavigationComponent(pageContext)%>" />

<!-- Navigation component -->
<!-- <div class="hider-wrapper">
    <div class="hider"></div>
</div> -->
<div class="primary-nav-wrapper">
    <nav class="primary-nav">
        <div class="window row">
            <div class="nav-wrapper">
                <ul class="nav-bar scrollable">
                    <c:forEach var="item" items="${model.items}">
                        <li class="${item.activeClass}"><a href="${item.link}">${item.title}</a></li>
                    </c:forEach>
                </ul>
                <div class="menu-button">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <span class="all caption">ALL</span>
                    <span class="close caption">CLOSE</span>
                </div>
            </div>
        </div>
    </nav>
    <nav class="expanded-nav">
        <ul class="nav-scroller">
            <c:forEach var="item" items="${model.items}">
            <li>
                <div class="row">
                    <div class="left-pane">
                        <a href="${item.link}">${item.title}</a>
                        <c:if test="${item.adPresent}">
                            <div class="sponsor">
                                <p>Brought to you by</p>
                                <a href="${item.adLink}"><img src="${item.adImagePath}" alt="" /></a>
                            </div>
                        </c:if>
                    </div>
                    <div class="right-pane">
                        <c:forEach var="row" items="${item.navigationItems}">
                            <ul>
                                <c:forEach var="rowItem" items="${row.navigationItems}">
                                    <li><a href="${rowItem.link}">${rowItem.title}</a></li>
                                </c:forEach>
                            </ul>
                        </c:forEach>
                        <c:if test="${item.sectionFooterPresent}">
                            <a href="${item.sectionFooterLink}" class="button-arrow"><span>${item.sectionFooterLabel}</span><svg class="icon icon-arrow-right"><use xlink:href="#icon-arrow-right"></use></svg></a>
                        </c:if>
                    </div>
                </div>
            </li>
            </c:forEach>
        </ul>
    </nav>
</div>