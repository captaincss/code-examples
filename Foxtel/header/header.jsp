<%-- FNA Header --%>

<%@page session="false" %>
<%@include file="/libs/foundation/global.jsp"%>

<%@page import="com.isobar.foxtel.components.header.HeaderComponent"%>
<c:set var="model" value="<%=new HeaderComponent(pageContext)%>" />

<%@page import="com.isobar.foxtel.components.config.GoogleCseConfigComponent"%>
<c:set var="cse" value="<%=new GoogleCseConfigComponent(pageContext)%>"/>

<!--[if lt IE 9]>
<div class="browser-upgrade">
	<p>You are using an <b>outdated</b> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> for the best experience.</p>
</div>
<![endif]-->

<!-- header component -->
<header class="header-main">
	<div id="search-container" class="search-wrap">
		<div class="initial">
			<a href="/" class="main-logo"><img src="${model.logo}" alt="${model.altText}" /></a>
			<label class="search-show">
				<svg class="icon icon-mag-glass"><use xlink:href="#icon-mag-glass"></use></svg>
			</label>
		</div>
		<div class="search">
			<form id="search-header" class="search-area" action="${cse.searchPagePath}.html">
				<label class="search-show">
					<svg class="icon icon-mag-glass"><use xlink:href="#icon-mag-glass"></use></svg>
				</label>
				<div class="search-field">
					<input id="txt-search-header" name="txt-search" class="typeahead" data-cseid="${cse.cx}" type="text" placeholder="Search" />
				</div>
				<button type="reset" class="btn-reset">
					<svg class="icon icon-close"><use xlink:href="#icon-close"></use></svg>
				</button>
				<input type="submit" class="hide" />
			</form>
		</div>
	</div>
</header>
