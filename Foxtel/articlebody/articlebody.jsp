<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"
%><%@include file="/apps/foundation/global.jsp"
%><%@page import="com.isobar.foxtel.components.article.ArticleComponent"%>
<c:set var="model" value="<%=new ArticleComponent(pageContext)%>"/>
<c:choose>
	<c:when test="${model.valid}">
		<c:if test="${model.imageVert eq 'bottom'}">${model.richtext}</c:if>
		<c:if test="${not empty model.imgPath}">
		<figure class="${model.imageCss}">
			<fox:img src="${model.imgPath}" alt="${model.imgAlt}" width="660" quality="medium"/>
			<figcaption>${model.imgDesc}</figcaption>
		</figure>
		</c:if>
		<c:if test="${model.imageVert eq 'top'}">${model.richtext}</c:if>
	</c:when>
	<c:when test="${not model.valid && model.isAuthorMode}">
		<div class="cq-title-placeholder"></div>
	</c:when>
</c:choose>