<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"
%><%@include file="/apps/foundation/global.jsp"
%><%@page import="com.isobar.foxtel.components.content.contentpillars.ContentPillarsComponent"
%><c:set var="model" value="<%=new ContentPillarsComponent(pageContext)%>"/>
<div class="container-single-column content-pillars">
	<div class="container-wrapper">
		<div class="container-content">
			<c:choose>
			<c:when test="${model.valid}">
			<header>
				<div class="content-pillars-heading"><c:out value="${model.title}"/></div>
				<c:if test="${not empty model.morePath}">
				<a href="${model.morePath}.html" class="button-arrow small mid-grey visible-large">
					<span><c:out value="${model.moreLabel}"/></span>
					<svg class="icon icon-arrow-right">
		    			<use xlink:href="#icon-arrow-right"></use>
		    		</svg>
				</a>
				</c:if>
			</header>
			<div class="row">
				<c:forEach var="item" items="${model.contentItems}" varStatus="index">
				<c:if test="${index.count == 1}">
				<article class="feature">
					<picture>
						<a href="${item.path}.html">
						<c:choose>
						<c:when test="${not empty item.imageLarge}">
							<img src="${item.imageLarge}">
						</c:when>
						<c:otherwise>
							<fox:img src="${item.image}" width="703" crop16x9="true" quality="medium" />
						</c:otherwise>
						</c:choose>
						</a>
					</picture>
					<div class="heading"><a href="${item.path}.html"><c:out value="${item.shortTitle}"/></a></div>
					<p><c:out value="${item.shortDesc}"/></p>
				</article>
				</c:if>
				<c:if test="${index.count > 1}">
				<article>
					<picture>
						<a href="${item.path}.html">
						<c:choose>
						<c:when test="${not empty item.imageMedium}">
							<img src="${item.imageMedium}">
						</c:when>
						<c:otherwise>
							<fox:img src="${item.image}" width="344" crop16x9="true" quality="medium" />
						</c:otherwise>
						</c:choose>
						</a>
					</picture>
					<div class="heading"><a href="${item.path}.html"><c:out value="${item.shortTitle}"/></a></div>
					<p><c:out value="${item.shortDesc}"/></p>
				</article>
				</c:if>
				</c:forEach>
			</div>
			<a href="${model.morePath}.html" class="button-arrow small mid-grey visible-small">
				<span><c:out value="${model.moreLabel}"/></span>
				<svg class="icon icon-arrow-right">
	    			<use xlink:href="#icon-arrow-right"></use>
	    		</svg>
			</a>
			</c:when>
			<c:when test="${not model.valid && model.isAuthorMode}">
				<div class="cq-title-placeholder" style="width:100%"></div>
			</c:when>
			</c:choose>
		</div>
	</div>
</div>