<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"
%><%@include file="/apps/foundation/global.jsp"
%><%@page import="com.isobar.foxtel.components.content.bookend.BookendComponent"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" 
%><c:set var="model" value="<%=new BookendComponent(pageContext)%>"/>
<div class="container-single-column bookend accent-quinary">
	<div class="container-wrapper">
		<div class="container-content">
			<c:choose>
			<c:when test="${model.valid}">
			<header>
				<div class="bookend-heading"><c:out value="${model.title}"/></div>
			</header>
			<div class="row">
				<c:if test="${fn:length(model.contentItems) > 0}">
				<article class="feature" data-height-match-large>
					<a href="${model.contentItems[0].path}.html">
						<picture class="arrow-right">
							<c:choose>
							<c:when test="${not empty model.contentItems[0].imageLarge}">
								<img src="${model.contentItems[0].imageLarge}">
							</c:when>
							<c:otherwise>
								<fox:img src="${model.contentItems[0].image}" width="545" crop16x9="true" quality="medium" />
							</c:otherwise>
							</c:choose>
						</picture>
						<div class="inner">
							<div class="label pull-up"><span><c:out value="${model.contentItems[0].contentType}" /></span></div>
							<div class="heading">${model.contentItems[0].shortTitle}</div>
						</div>
					</a>
				</article>
				</c:if>
				<c:if test="${fn:length(model.contentItems) > 1}">
				<article data-height-match>
					<a href="${model.contentItems[1].path}.html">
						<div class="inner">
							<div class="label"><span><c:out value="${model.contentItems[1].contentType}" /></span></div>
							<div class="heading restrict-height">${model.contentItems[1].shortTitle}</div>
							<p>${model.contentItems[1].description}</p>
						</div>
					</a>
				</article>
				</c:if>
				<c:if test="${fn:length(model.contentItems) > 2}">
				<article class="four-tile" data-height-match-small>
					<div class="item">
						<a href="${model.contentItems[2].path}.html">
							<div class="inner">
								<div class="label"><span><c:out value="${model.contentItems[2].contentType}" /></span></div>
								<div class="heading">${model.contentItems[2].shortTitle}</div>
							</div>
							<picture class="arrow-left">
								<c:choose>
								<c:when test="${not empty model.contentItems[2].imageMedium}">
									<img src="${model.contentItems[2].imageMedium}">
								</c:when>
								<c:otherwise>
									<fox:img src="${model.contentItems[2].image}" width="273" crop16x9="true" quality="medium" />
								</c:otherwise>
								</c:choose>
							</picture>
						</a>
					</div>
					<c:if test="${fn:length(model.contentItems) > 3}">
					<div class="item">
						<a href="${model.contentItems[3].path}.html">
							<picture class="arrow-right">
								<c:choose>
								<c:when test="${not empty model.contentItems[3].imageMedium}">
									<img src="${model.contentItems[3].imageMedium}">
								</c:when>
								<c:otherwise>
									<fox:img src="${model.contentItems[3].image}" width="273" crop16x9="true" quality="medium" />
								</c:otherwise>
								</c:choose>
							</picture>
							<div class="inner">
								<div class="label"><span><c:out value="${model.contentItems[3].contentType}" /></span></div>
								<div class="heading">${model.contentItems[3].shortTitle}</div>
							</div>
						</a>
					</div>
					</c:if>
				</article>
				</c:if>
				<c:if test="${fn:length(model.contentItems) > 4}">
				<article class="feature">
					<a href="${model.contentItems[4].path}.html">
						<picture class="arrow-right">
							<c:choose>
							<c:when test="${not empty model.contentItems[4].imageLarge}">
								<img src="${model.contentItems[4].imageLarge}">
							</c:when>
							<c:otherwise>
								<fox:img src="${model.contentItems[4].image}" width="545" crop16x9="true" quality="medium" />
							</c:otherwise>
							</c:choose>
						</picture>
						<div class="inner">
							<div class="label pull-up"><span><c:out value="${model.contentItems[4].contentType}" /></span></div>
							<div class="heading">${model.contentItems[4].shortTitle}</div>
						</div>
					</a>
				</article>
				</c:if>
			</div>
			</c:when>
			<c:when test="${not model.valid && model.isAuthorMode}">
				<div class="cq-title-placeholder"></div>
			</c:when>
			</c:choose>
		</div>
	</div>
</div>