/**
 * Bookend related
 *
 */
if (FT.exists(FT.COMPONENTS.BOOKEND)) {
	$(document).ready(function () {
		FT.Bookend = (function () {
			'use strict';

			var dataMatchEle = $('[data-height-match]'),
				dataSmall = 'height-match-small',
				dataLarge = 'height-match-large';

			var setHeight = function () {
				if (window.innerWidth <= FT.SMALL_BREAKPOINT) {
					dataMatchEle.css('height', '');
				} else {
					if (FT.isSmallBreakpoint() || FT.isMediumBreakpoint()) {
						var matchHeight = $('[data-' + dataSmall + ']').height();
						dataMatchEle.css('height', matchHeight);
					} else if (FT.isMediumUpBreakpoint()) {
						var matchHeight = $('[data-' + dataLarge + ']').innerHeight();
						dataMatchEle.css('height', matchHeight);
					}
				}
			};

			var setupEvents = function () {
				setHeight();
				$(window).on('resize', FT.debounce(function () {
					setHeight();
				}));
				$(window).on('load', function () {
					setHeight();
				})
			};

			return {
				init: function () {
					setupEvents();
				}
			};
		})();

		FT.Bookend.init();
	});
}