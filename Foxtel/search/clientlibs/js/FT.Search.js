/**
 * This module has been wrapped with FT memory mangement logic.
 */
if (FT.exists(FT.COMPONENTS.SEARCH)) {
	$(document).ready(function () {

		/**
		 * Code to control the search component
		 * @namespace Search
		 * @memberof FT
		 * @author Kevin Jarvis
		 * @requires JQuery
		 * @requires Typeahead
		 */
		FT.Search = (function () {
			'use strict';

			var headerClass = '.header-main',
				searchContainer = $('#search-container'),
				searchContent = $('#search-content'),
				searchActivate = $('#search-container .search-show'),
				searchClass = '.search',
				searchReset = $('.btn-reset'),
				searchResetClass = '.btn-reset',
				txtSearch = $('.typeahead'),
				txtSearchClass = '.typeahead',
				activeClass = 'search-active',
				searchResultsContainer = $('.x-search-results'),
				cseid = txtSearch.data('cseid'),
				SEARCH_REQUEST_URI = 'http://clients1.google.com/complete/search?client=partner&ds=cse&q=%QUERY&partnerid=' + cseid,
				searchFilter = $('.search-filter');


			/**
			 * Setup click events
			 * @memberof FT.Search
			 * @private
			 * @return {undefined}
			 */
			var setupEvents = function () {

				/**
				 * Bloodhound is typeaheads suggestion engine (includes, cachine, throttling etc)
				 */
				var results = new Bloodhound({
					name: 'search-results',
					datumTokenizer: function (d) {
						return Bloodhound.tokenizers.whitespace(d.val);
					},
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					remote: {
						url: SEARCH_REQUEST_URI,
						ajax: {
							dataType: 'jsonp'
						},
						filter: function (data) {
							var items = $.map(data[1], function (item) {
								return {
									value: item
								};
							});
							return items;
						}
					}
				});

				results.initialize();

				// Typeahead (Plugin) setup, currently all inputs share same data source
				$('#search-header input.typeahead, #search-content-page input.typeahead').typeahead({
					minLength: 2,
					hightlight: true,
					hint: true
				}, {
					displayKey: 'value',
					source: results.ttAdapter()
				}).on('typeahead:selected', function () {
					FT.log('open');
					$(this).parents('form')[0].submit();
				}).each(function () {
					if ($(this).typeahead('val') !== '') {
						$(this).parents('form').find(searchResetClass).addClass('clear');
					}
				});

				// Only applicable to header search
				// When search animates in, focus text field
				/*$('#search-container .search').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
					if (e.target.className === 'search') {
						if ($('body').hasClass(activeClass)) {
							$(this).find(txtSearchClass).focus().select();
						}
					}
				});*/

				// Only applicable to header search
				// Toggle reset button styling depending on if input has a value
				txtSearch.on('change keyup', function (e) {
					var form = $(this).parents('form')[0],
						resetBtn = $(form).find(searchResetClass);

					if ($(this).typeahead('val') === '') {
						resetBtn.removeClass('clear');
					} else {
						resetBtn.addClass('clear');
					}

					if (e.keyCode == 13) {
						$(form).submit();
					}
				});


				// Only applicable to header search
				// Need to apply overflow hidden/visible on search hide/show
				/*searchContainer.find(txtSearchClass).on('focus', function () {
					$(headerClass + ', ' + headerClass + ' .search').css('overflow', 'visible');
				});

				// Only applicable to header search
				// Need to apply overflow hidden/visible on search hide/show
				searchContainer.find(txtSearchClass).on('blur', function () {
					$(headerClass + ', ' + headerClass + ' .search').css('overflow', 'hidden');
				});*/

				// Only applicable to header search
				// Upon clicking activate icon, show/hide header search
				searchActivate.on('click', function (e) {
					if (!$('body').hasClass(activeClass)) {
						showSearch();
						$(searchContainer).find(txtSearchClass).select();
						e.preventDefault();
						/*setTimeout(function () {

						}, 350);*/
					}
				});

				// Only applicable to header search
				// Upon clicking page overlay, hide search
				$('.blurred-content').on('click', function () {
					$(searchContainer).find(txtSearchClass).blur();
					hideSearch();
				});

				// This either hides search if input has no value OR clears input
				$(searchClass + ' ' + searchResetClass).on('click', function (e) {
					var taInput = $(this).parents('form').find(txtSearchClass);
					if (taInput.typeahead('val') === '') {
						hideSearch();
					} else {
						taInput.typeahead('val', '');
						taInput.trigger('change');
					}
					e.preventDefault();
				});

				var initializeScroller = function () {
					// Initialize IScroll for search filter
					if (typeof IScroll !== 'undefined') {
						searchFilter.each(function () {
							var parentEle = $(this).parent()[0],
								totalWidth = 0,
								options = {
									scrollX: true, // enable left/right scrolling
									scrollY: false, // disable Y scrolling
									click: true // required to allow click events on mobile devices
								};

							$(this).find('li').each(function () {
								totalWidth += $(this).innerWidth();
							});
							$(this).width(totalWidth);

							new IScroll(parentEle, options);
						});
					}
				};

				initializeScroller();

				//}


				// tabs
				$('.search-filter').on('click', 'a', function (event) {
					event.preventDefault();
					var $link = $(this);
					var href = $link.attr('href');

					$link.parents('ul').first().find('.active').removeClass('active');
					$link.parent().addClass('active');

					$.ajax({
						url: href,
						cache: false,
						success: function (data) {
							searchResultsContainer.html(data);
						}
					});
				});

				// ordering
				searchResultsContainer.on('click', '.sorting a', function (event) {
					event.preventDefault();
					var $link = $(this);
					var href = $link.attr('href');
					$.ajax({
						url: href,
						cache: false,
						success: function (data) {
							searchResultsContainer.html(data);
						}
					});

				});

			};


			/**
			 * Show the search bar
			 * @memberof FT.Search
			 * @private
			 * @return {undefined}
			 */
			var showSearch = function () {
				$('body').addClass(activeClass);
			};


			/**
			 * Hide the search bar
			 * @memberof FT.Search
			 * @private
			 * @return {undefined}
			 */
			var hideSearch = function () {
				$('body').removeClass(activeClass);
			};


			/**
			 * Standard entry point for JS modules. The constructor is always the last function in a module.
			 * @memberof FT.Search
			 * @private
			 * @return {undefined}
			 */
			var constructor = (function () {
				setupEvents();
			})();


			return {};
		})();
	});
}