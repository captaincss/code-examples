<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@include file="/apps/foundation/global.jsp"%>
<c:set var="model" value="<%=new com.isobar.foxtel.components.search.Search(pageContext)%>" />
<div class="search-page">
	<section class="container-large-body-sidebar">
		<div class="container-background">
			<div class="container-wrapper">
				<div class="container-content no-pad-bot-lrg">
					<div class="container-large-body">
						<span class="heading-resultsfor">Results for...</span>
						<div class="search-area">
							<form id="search-content-page" class="search" action="${currentPage.path}.html">
								<label class="search-show" for="txt-search-page">
									<svg class="icon icon-mag-glass"><use xlink:href="#icon-mag-glass"></use></svg>
								</label>
								<div class="search-field">
									<input id="txt-search" name="txt-search" class="typeahead" type="text" placeholder="Search"
										data-cseid="{model.cseId}"
										value="<c:out value="${model.searchForm.text}"/>" />
								</div>
								<button type="reset" class="btn-reset">
									<svg class="icon icon-close"><use xlink:href="#icon-close"></use></svg>
								</button>
								<input type="submit" class="hide" />
								<input name="_charset_" type="hidden" value="utf-8" />
							</form>

							<!-- suggestions box -->
							<c:if test="${not empty model.suggestionLink}">
								<div class="search-correction">
									<div class="inner-wrap">
										<div class="left-col">
											<p>Corrected: Showing results for: <i><c:out value="${model.currentQuery}"/></i></p>
											<p>Search instead for: <a href="${model.suggestionLink.path}"><i><c:out value="${model.suggestionLink.title}"/></i></a></p>
										</div>
										<p class="number-results">${model.totalResults} results</p>
									</div>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


		<!-- facet tabs -->
		<section class="container-full-width-single-column no-bottom-spacing no-top-spacing">
			<div class="container-background">
				<div class="container-wrapper">
					<div class="filter-wrapper">
						<ul class="search-filter">
							<c:forEach var="facet" items="${model.facetLinks}">
								<li class="${facet.current ? 'active' : ''}"><a href="${facet.path}"><span>${facet.title}</span></a></li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<div class="x-search-results">
		<%@ include file="includes/results.jspf" %>
		</div>
</div>

<!-- also interested -->
<!-- out of scope -->
<!--
<section class="container-split-two-four">
	<div class="container-wrapper">
		<div class="container-background">
			<div class="container-content search-additional">
				<h4>You may also be interested in...</h4>
				<ul class="container-split-body">
					<li>
						<a href="#"><img src="/static-images/also-interested.jpg" alt="" /></a>
						<h5><a href="#">Top of the Lake</a></h5>
						<p>2013 television miniseries written by Jane Campion and Gerard Lee, and directed by Campion and Garth Davis.</p>
					</li>
					<li>
						<a href="#"><img src="/static-images/also-interested.jpg" alt="" /></a>
						<h5><a href="#">True Detective</a></h5>
						<p>True Detective is an American television crime drama series on HBO created and written by Nic Pizzolatto.</p>
					</li>
					<li>
						<a href="#"><img src="/static-images/also-interested.jpg" alt="" /></a>
						<h5><a href="#">Fargo</a></h5>
						<p>Fargo is an American dark comedy-crime drama television series created and written by Noah Hawley. The show is inspired by the 1996 film of.</p>
					</li>
					<li>
						<a href="#"><img src="/static-images/also-interested.jpg" alt="" /></a>
						<h5><a href="#">Luther</a></h5>
						<p>AFL scouts were yesterday briefed on the trailblazing format, which will take place at Crown on September 17.</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
-->

