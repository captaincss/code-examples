<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page    import="com.isobar.foxtel.components.navigation.SecondaryNavigationComponent"%>

<c:set var="model" value="<%=new SecondaryNavigationComponent(pageContext)%>" />

<!-- Secondary Navigation component -->
<c:if test="${!model.currentSecNavHidden}">
<div class="primary-nav-wrapper navigation">
    <nav class="primary-nav secondary-nav">
        <div class="window row">
            <div class="nav-wrapper">
                <ul class="nav-bar scrollable">
                    <c:forEach var="item" items="${model.items}">
                    	<li class="${item.activeClass}"><a href="${item.link}">${item.title}</a></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </nav>
    <c:if test="${not empty model.currentItem && not empty model.currentItem.navigationItems}">
        <nav class="primary-nav tertiary-nav">
            <div class="window row">
                <a href="#" class="show-more">More in this section</a>
                <ul class="nav-bar">
                    <c:forEach var="item" items="${model.currentItem.navigationItems}">
                        <li class="${item.activeClass}"><a href="${item.link}">${item.title}</a></li>
                    </c:forEach>
                </ul>
            </div>
        </nav>
    </c:if>
</div>
</c:if>