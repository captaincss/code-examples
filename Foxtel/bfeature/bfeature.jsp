<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@include file="/apps/foundation/global.jsp"%>
<%@page import="com.isobar.foxtel.components.bfeature.BFeatureComponent"%>
<c:set var="model" value="<%=new BFeatureComponent(pageContext)%>"/>

<div class="container-wrapper">
	<div class="container-content">
		<div class="row">
			<c:forEach var="item" items="${model.items}">
				<article>
					<c:choose>
						<c:when test="${item.valid}">
							<fox:a href="${item.path}">
								<picture>
									<fox:img src="${item.image}" width="580" crop16x9="true" quality="medium" />
								</picture>
								<div class="label"><span>${item.type}</span></div>
								<p>${item.text}</p>
							</fox:a>
						</c:when>
						<c:when test="${not item.valid && model.isAuthorMode}">
							<div class="cq-title-placeholder" style="width:100%"></div>
						</c:when>	
					</c:choose>
				</article>
			</c:forEach>
		</div>
	</div>
</div>
