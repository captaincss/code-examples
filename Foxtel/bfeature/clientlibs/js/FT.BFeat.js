/**
 * B Feature stuff
 */
if (FT.exists(FT.COMPONENTS.BFEATURE)) {
	$(document).ready(function () {

		/**
		 * Helper functions for B Feature
		 * @namespace BFeat
		 * @memberof FT
		 * @author Mark Boere
		 * @requires JQuery
		 */
		FT.BFeat = (function () {
			'use strict';

			/**
			 * Equalise height of elements on resize
			 * @memberof FT.Form
			 * @private
			 * @return {undefined}
			 */
			var equaliseHeights = function () {
				var equalise = function (elems) {
					var $elems = $(elems),
						heighest = 0;

					$elems.height('auto').each(function () {
						if ($(this).height() > heighest) {
							heighest = $(this).height();
						}
					});

					if (window.innerWidth > FT.SMALL_BREAKPOINT) {
						$elems.height(heighest);
					} else {
						$elems.height('auto');
					}
				};

				$(window).resize(FT.debounce(function () {
					equalise('.bfeature .row article p');
				})).resize();
			};



			return {
				/**
				 * Standard entry point for site components
				 * @return {undefined}
				 */
				init: function () {
					equaliseHeights();
				}
			};
		})();
		FT.BFeat.init();
	});
}